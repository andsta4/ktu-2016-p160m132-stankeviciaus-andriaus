Common Command Line Based Utilities for Data Science
====================================================

Recommended installation method to Python environment:
``python setup.py develop``

See output of ``--help`` flag for scripts in ``bin``.
