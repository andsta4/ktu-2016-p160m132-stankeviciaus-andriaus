import itertools


__all__ = ["mk_param_grid_from_args", "mk_param_grid"]


def mk_param_grid_from_args(name_values_str_pairs, delimiter):
    return mk_param_grid(
	    [(k, list(map(_to_num, vs.split(delimiter))),)
	     for k, vs in name_values_str_pairs])


def mk_param_grid(name_values_pairs):
    name_value_pairs = [
        [(k, v,) for v in vs]
        for k, vs in name_values_pairs]
    return list(itertools.product(*name_value_pairs))


def _to_num(x):
    try:
        val = float(x)
    except ValueError:
        val = x
    return val
