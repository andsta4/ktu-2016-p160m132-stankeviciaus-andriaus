import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()

requires = [
    'docopt'
    ]

setup(name='ds_utils',
      version='0.0.1',
      description='Common CLI based utilities for data science.',
      long_description=README,
      classifiers=[
          "Programming Language :: Python3",
      ],
      author='Darius Aliulis',
      author_email='darius.aliulis@ktu.lt',
      url='',
      keywords='cli util utilities tools',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      scripts=[
          "bin/cat_csv",
          "bin/mk_param_grid",
          ])
