import sys


def mk_input_source(filename, script_name=None):
    if script_name is None:
        script_name = __file__
    if filename and filename != "-":
        return open(filename, encoding="utf-8")
    elif not sys.stdin.isatty():
        return sys.stdin
    else:
        sys.exit("{}: nothing to read from stdin".format(script_name))


def mk_output_sink(filename, mode="w"):
    return open(filename, "w", encoding="utf-8") if filename else sys.stdout
