# Sudaro 2 standartizuotus spark csv: train ir test; ir standartizavimo statistiku failus: mean ir std

import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext
from src.standardization_functions import scaleData

from pyspark import SparkContext
from pyspark.sql import SQLContext, Row

if __name__ == "__main__":
    if len(sys.argv) != 9:
        raise ValueError('Reikia visu verciu: duomenuTrainFailas,duomenuTestFailas,failoTrainHeader,failoTestHeader, continuousStulpeliuSarasoFailas, binarySarasas' +\
                        ', direktorijaKurRasytiFailus, kaipPavadintiFailus')

    sc = SparkContext(master="local", appName="StandardizationProcess")
    sqlContext = SQLContext(sc)

    dataTrainPath = makePathFromPythonContext(sys.argv[1])
    dataTrainHeaderPath = makePathFromPythonContext(sys.argv[2])
    dataTestPath = makePathFromPythonContext(sys.argv[3])
    dataTestHeaderPath = makePathFromPythonContext(sys.argv[4])
    continuousPath = makePathFromPythonContext(sys.argv[5])
    binaryPath = makePathFromPythonContext(sys.argv[6])
    outputPath = makePathFromPythonContext(sys.argv[7])
    outputFileName = sys.argv[8]

    # Failo header uzkrovimas
    with open(dataTrainHeaderPath) as f:
        trainDataColumns = f.read().split(",")
    with open(dataTestHeaderPath) as f:
        testDataColumns = f.read().split(",")
    # Failo uzkrovimas
    trainData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataTrainPath)
        .rdd
        .toDF(trainDataColumns)
    )
    testData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataTestPath)
        .rdd
        .toDF(testDataColumns)
    )
    # Stulpeliu tipu sarasu uzkrovimas
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")
    with open(binaryPath) as f:
        binaryColumns = f.read().split(",")
    
    # Nustatomi turimi stulpeliai
    continuousColumns = [c for c in continuousColumns if c in trainDataColumns]
    
    # Istrinamas outputPath direktorija, jei tokia yra, uztikrinti, kad bus sparkcsv teisingai irasyti
    shutil.rmtree(outputPath, ignore_errors=True)
    
    # Sukurimas output dir
    os.makedirs(outputPath)
    
    # Atliekama standartizacija Train rinkiniui
    (scalerModel, scaledTrainData) = scaleData(trainData, continuousColumns)
    
    # Irasomos standartizuotu kintamuju statistikos
    scalerMean = " ".join(map(str, scalerModel.mean))
    scalerStd = " ".join(map(str, scalerModel.std))
    with open(os.path.join(outputPath, "usage_{0}_scaler__mean.txt".format(outputFileName)), "w") as f:
        f.write(scalerMean)
    with open(os.path.join(outputPath, "usage_{0}_scaler__std.txt".format(outputFileName)), "w") as f:
        f.write(scalerStd)
        
    # Irasomi standartizuoti duomenys
    scaledRow = Row(*(["user_account_id"] + continuousColumns))
    # Train duomenys
    scaledTrainData = (
        scaledTrainData.select("user_account_id", "scaled_features")
        .rdd
        .map(lambda row: scaledRow(row.user_account_id,  *map(float, row.scaled_features.toArray())))
        .toDF()
    )
    # Prijungiami binary stulpeliai
    binaryTrainData = trainData.select(['user_account_id'] + binaryColumns)
    scaledTrainData = scaledTrainData.join(binaryTrainData, ['user_account_id'], 'inner')
    # Standartizuotų duomenų header įrašomas į failą, kad būtų
    # galima įkelti su stulpelių pavadinimais. Taip pat įrašomas pats duomenų failas
    # Jeigu jau bus toks failas jis bus perrasomas
    with open(os.path.join(outputPath,
            "header__usage_train_scaled_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(scaledTrainData.columns))
    scaledTrainData.write.format("com.databricks.spark.csv").save(
        os.path.join(outputPath,"usage_train_scaled_{}/".format(outputFileName)))
    
    
    # Atliekama standartizacija TEST rinkiniui
    (_ , scaledTestData) = scaleData(trainData, continuousColumns, scalerModel)
        
    # Train duomenys
    scaledTestData = (
        scaledTestData.select("user_account_id", "scaled_features")
        .rdd
        .map(lambda row: scaledRow(row.user_account_id,  *map(float, row.scaled_features.toArray())))
        .toDF()
    )
    binaryTestData = testData.select(['user_account_id'] + binaryColumns)
    scaledTestData = scaledTestData.join(binaryTestData, ['user_account_id'], 'inner')
    
    # Standartizuotų duomenų header įrašomas į failą, kad būtų
    # galima įkelti su stulpelių pavadinimais. Taip pat įrašomas pats duomenų failas
    # Jeigu jau bus toks failas jis bus perrasomas
    with open(os.path.join(outputPath,
            "header__usage_test_scaled_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(scaledTestData.columns))
    scaledTestData.write.format("com.databricks.spark.csv").save(
        os.path.join(outputPath,"usage_test_scaled_{}/".format(outputFileName)))

    sc.stop()
