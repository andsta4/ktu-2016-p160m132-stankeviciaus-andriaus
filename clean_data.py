# -*- coding: utf-8 -*-
# Failas clean_data.py
# Spark job python scriptas skirtas atmesti nereikalingas eilutes ir daryti
# kitas duomenų valymo operacijas pagal apžvalgos metu nuspręstus medotus.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 3:
        raise ValueError('''Reikia visu verciu: direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')
    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]", appName="DataCleaningProcess")
    sqlContext = SQLContext(sc)
    # Užkraunami parametrai iš konsolės.
    outputPath = makePathFromPythonContext(sys.argv[1])
    outputFileName = sys.argv[2]
    # Failo užkrovimas.
    usageDF = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True, header=True, nullValue="")
        .load(os.path.join(os.getcwd(), "data/customer_usage_00025.csv"))
    )
    # Panaikinamos pasikartojancios eilutės.
    usageDF = usageDF.drop_duplicates()

    usageDf = (
        usageDF
        # Panaikinami neigiamus duomenis turintys įrašai.
        .filter('last_100_reloads_sum >= 0.0')
        .filter('gprs_spendings >= 0.0')
        .filter('sms_incoming_from_abroad_spendings >= 0.0')
        .filter('sms_incoming_spendings >= 0.0')
        .filter('sms_outgoing_to_offnet_spendings >= 0.0')
        .filter('sms_outgoing_spendings >= 0.0')
        .filter('reloads_sum >= 0.0')
        .filter('user_spendings >= 0.0')
        # Panaikinami pastebėtos, manomos, išskirtys.
        # Nors ir kai kurios reikšmės yra vertingų vartotojų indikatoriai.
        # Jos vistiek bus panaikinamos.
        #Nes joms nesigaus generalizuoto modelio ir tiktai bus gaunamas
        #prastesnis
        #modelis kitiems įrašams klasifikuoti.
        .filter('calls_outgoing_to_abroad_duration < 300')
        .filter('calls_outgoing_to_abroad_spendings < 100')
        .filter('calls_outgoing_spendings_max < 20')
        .filter('calls_outgoing_spendings < 150')
        .filter('calls_outgoing_count < 1000')
        .filter('reloads_sum < 200')
        # kas sąskaita pildosi 5+ kart per mėnesį?????
        .filter('reloads_count < 5')
        .filter('user_spendings < 300')
        # Tokiems vertingiems klientams reiktų taisyklėmis grįsto modelio,
        # o ne generalizuoto.
        .filter('user_account_balance_last < 200')
        # Šitie jau nebesinaudoja paslaugomis ilgesniu negu 3 mėnesių
        # laikotarpiu.
        .filter('user_no_outgoing_activity_in_days < 100')
        .filter('last_100_sms_outgoing_to_abroad_count < 300')
        .filter('last_100_gprs_usage < 2000')
        # Jeigu, kas 5 dienas pildosi sąskaitą, tai nelabai tikėtina, kad yra
        # nepatenkinti paslauga.
        .filter('last_100_reloads_count < 20')
        .filter('gprs_spendings < 50')
    )

    # Paruošiama direktorija išvedimui.
    shutil.rmtree(outputPath, ignore_errors=True)
    os.makedirs(outputPath)
    # Išvedami rezultatai.
    with open(os.path.join(outputPath,
        "header__cleaned_usage_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(usageDF.columns))
    usageDF.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath,"cleaned_usage_{}".format(outputFileName))
    )

    sc.stop()
