#!/usr/bin/env bash

# sukuriamos direktorijos duomenu paruosimui
mkdir output
mkdir output/preparation

# sukuriamos direktorijos modeliavimui pasitelkiant klasterizacija bei klasifikacija
mkdir models
mkdir models/clustering
mkdir models/classification

# sukuriamos direktorijos rezultatu failams
mkdir results_exploratory
mkdir results_clustering
mkdir results_classification

# sukuriamos direktorijos klasifikacijos modeliams
mkdir models/classification/randomForest
mkdir models/classification/decisionTree
mkdir models/classification/logisticRegression
