# -*- coding: utf-8 -*-
# Failas: fix_clusters.py
# Panaikina pasirinktus klasterius arba sujungia kelių duomenis į bendresnius

import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext
from src.standardization_functions import scaleData

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import col

if __name__ == "__main__":
    if len(sys.argv) != 11:
        raise ValueError('''Reikia visu verciu: duomenuTrainFailas,
                failoTrainHeader,duomenuTestFailas,failoTestHeader
                , kmeansRmclusters, kmeansConnectCluster, gaussRm,
                gaussCon, direktorijaKurRasytiFailus,
                kaipPavadintiFailus''')

    # Nustatymai sparko aplinkai
    sc = SparkContext(master="local", appName="ClusteringModificationProcess")
    sqlContext = SQLContext(sc)
    # Nuskaitomi reikalingi parametrai iš konsolės.
    dataTrainPath = makePathFromPythonContext(sys.argv[1])
    dataTrainHeaderPath = makePathFromPythonContext(sys.argv[2])
    dataTestPath = makePathFromPythonContext(sys.argv[3])
    dataTestHeaderPath = makePathFromPythonContext(sys.argv[4])
    kmeansRmList = sys.argv[5]
    kmeansConList = sys.argv[6]
    gaussianRmList = sys.argv[7]
    gaussianConList = sys.argv[8]
    outputPath = makePathFromPythonContext(sys.argv[9])
    outputFileName = sys.argv[10]
    # Failų stulpelių pavadinimų užkrovimas.
    with open(dataTrainHeaderPath) as f:
        trainDataColumns = f.read().split(",")

    with open(dataTestHeaderPath) as f:
        testDataColumns = f.read().split(",")

    # Failų užkrovimas.
    trainData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataTrainPath)
        .rdd
        .toDF(trainDataColumns)
    )
    testData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataTestPath)
        .rdd
        .toDF(testDataColumns)
    )
    # Ištrinama išvedimo direktorija, jeigu jau tokia buvo prieš tai.
    # Reikia spark.csv tikriausiai.
    shutil.rmtree(outputPath, ignore_errors=True)
    # Sukurimas output dir.
    os.makedirs(outputPath)
    # Atmetamos pasirinktos k-vidurkių klasterizacijos klasterių eilutės.
    kmeansRmList = kmeansRmList.strip().split(',')
    if kmeansRmList[0] != '' and kmeansRmList[0] != 'no':
        for i in kmeansRmList:
            cluster = int(i)
            trainData = trainData.filter(col('pred_kmeans_mllib') != cluster)
            testData = testData.filter(col('pred_kmeans_mllib') != cluster)

    # Atmetamos pasirinktos Gauso mišinių klasterizacijos klasterių eilutės.
    gaussianRmList = gaussianRmList.strip().split(',')
    if gaussianRmList[0] != '' and gaussianRmList[0] != 'no':
        for i in gaussianRmList:
            cluster = int(i)
            trainData = trainData.filter(col('pred_gaussian_mllib') != cluster)
            testData = testData.filter(col('pred_gaussian_mllib') != cluster)

    # Sujungiamos nurodytos k-vidurkių klasterizacijos eilutės į nurodytą klasterį.
    kmeansConList = kmeansConList.strip().split(',')
    if len(kmeansConList) > 1:
        kmeansConList = list(map(int,kmeansConList))
        trainData = trainData.replace(kmeansConList[1:],
                      kmeansConList[0],'pred_kmeans_mllib')
        testData = testData.replace(kmeansConList[1:],
                      kmeansConList[0],'pred_kmeans_mllib')

    # Sujungiamos nurodytos Gauso mišinių klasterizacijos eilutės į nurodytą klasterį.
    gaussianConList = gaussianConList.strip().split(',')
    if len(gaussianConList) > 1:
        gaussianConList = list(map(int,gaussianConList))
        for i in gaussianConList:
            trainData = trainData.replace(gaussianConList[1:],
                    gaussianConList[0],'pred_gaussian_mllib')
            testData = testData.replace(gaussianConList[1:],
                    gaussianConList[0],'pred_gaussian_mllib')

    # Surašomi modifikuoti duomenys į pateiktą output direktoriją.
    with open(os.path.join(outputPath,
            "header__usage_train_scaled_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(trainData.columns))

    trainData.write.format("com.databricks.spark.csv").save(
        os.path.join(outputPath,"usage_train_scaled_{}/".format(outputFileName)))

    with open(os.path.join(outputPath,
            "header__usage_test_scaled_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(testData.columns))

    testData.write.format("com.databricks.spark.csv").save(
        os.path.join(outputPath,"usage_test_scaled_{}/".format(outputFileName)))

    sc.stop()
