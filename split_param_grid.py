# -*- coding: utf-8 -*-
# Failas: split_param_grid.py
# Paprastas python script skirtas suskaidyti sugeneruotą json objektą laikantį
# skirtingas hiperparametrų vertes į atskiras dalis.
import sys
import os
import json

from src.utility_functions import makePathFromPythonContext, splitJsonParamGrid

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(sys.argv)
        raise ValueError("Reikia visu: paramGrid, jsonFailoPath, ouputPath")
    
    paramGridPath = makePathFromPythonContext(sys.argv[1])
    outputPath = makePathFromPythonContext(sys.argv[2])
    splitJsonParamGrid(paramGridPath,outputPath)
    os.system("tree {}".format(outputPath))
