# -*- coding: utf-8 -*-
# Failas make_classification_data.py
# Spark job python script skirtas sujungti turimiems klasterizacijos
# rezultatams, atsako kintamajam ir kitiems kintamiesiems į duomenų failą
# skirtą klasifikacijai.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 12:
        raise ValueError('''Reikia visu verciu: duomenuTrainFailas, trainHeader
                , duomenuTestFailas, testHeader, churnFailas,
                kmeansRezultatuFailas, kmeansHeader, gaussianRezultatuFailas,
                gaussianHeader direktorijaKurRasytiFaila, kaipPavadintiFaila''')
    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]",
            appName="AddingClustersLabelsToDataProcess")
    sqlContext = SQLContext(sc)
    # Parametrai iš konsolės.
    trainDataPath = makePathFromPythonContext(sys.argv[1])
    dataTrainHeaderPath = makePathFromPythonContext(sys.argv[2])
    testDataPath = makePathFromPythonContext(sys.argv[3])
    dataTestHeaderPath = makePathFromPythonContext(sys.argv[4])
    labelDataPath = makePathFromPythonContext(sys.argv[5])
    kmeansDataPath = makePathFromPythonContext(sys.argv[6])
    kmeansDataHeaderPath = makePathFromPythonContext(sys.argv[7])
    gaussianDataPath = makePathFromPythonContext(sys.argv[8])
    gaussianDataHeaderPath = makePathFromPythonContext(sys.argv[9])
    outputPath = makePathFromPythonContext(sys.argv[10])
    outputFileName = sys.argv[11]

    # Failų stulpelių pavadinimų užkrovimas.
    with open(dataTrainHeaderPath) as f:
        trainDataColumns = f.read().split(",")
    with open(dataTestHeaderPath) as f:
        testDataColumns = f.read().split(",")
    with open(kmeansDataHeaderPath) as f:
        kmeansDataColumns = f.read().split(",")
    with open(gaussianDataHeaderPath) as f:
        gaussianDataColumns = f.read().split(",")
    # Duomenų rinkinių užkrovimas.
    trainData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(trainDataPath)
        .rdd
        .toDF(trainDataColumns)
    )
    testData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(testDataPath)
        .rdd
        .toDF(testDataColumns)
    )
    labelData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True, header=True)
        .load(labelDataPath)
    )
    labelData = labelData.select(['user_account_id', 'churn'])
    # Klasterizacijos rezultatų užkrovimas.
    kmeansData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(kmeansDataPath)
        .rdd
        .toDF(kmeansDataColumns)
    )
    gaussianData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(gaussianDataPath)
        .rdd
        .toDF(gaussianDataColumns)
    )
    # Sutvarkoma direktorija rezultatų išvedimui.
    shutil.rmtree(outputPath, ignore_errors=True)
    os.makedirs(outputPath)
    # Sujungiami duomenų rinkiniai pagal user_account_id vertes.
    # Sudaromas apmokymo rinkinys.
    trainData = trainData.join(labelData,['user_account_id'],"inner")
    trainData = trainData.join(kmeansData,['user_account_id'],"inner")
    trainData = trainData.join(gaussianData,['user_account_id'],"inner")
    # Sudaromas patvirtinimo rinkinys.
    testData = testData.join(labelData,['user_account_id'],"inner")
    testData = testData.join(kmeansData,['user_account_id'],"inner")
    testData = testData.join(gaussianData,['user_account_id'],"inner")
    # Išvedami rezultatai.
    with open(os.path.join(outputPath,
        "header__agg_usage_train_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(trainData.columns))
    trainData.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath, "usage_train_{}".format(outputFileName)))
    with open(os.path.join(outputPath,
        "header__agg_usage_test_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(testData.columns))
    testData.write.format("com.databricks.spark.csv").save(
                os.path.join(outputPath, "usage_test_{}".format(outputFileName))
                )

    sc.stop()
