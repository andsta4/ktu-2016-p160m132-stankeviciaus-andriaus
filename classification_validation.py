#-*- coding: utf-8 -*-
# file: classification_validation.py
# Čia yra pateikiamas spark job script, kuriame yra atliekama
# pasirinktos klasterizacijos suskirstytiems duomenims
# geriausių parametrų pasirinktinam klasifikacijos modeliui
# paieška.

# Moduliai reikalingi failų operacijoms.
import os
import sys
import json

# Moduliai skirti skaičiavimams
import src.utility_functions as util_fnc
import src.classification_functions as class_fnc

# Moduliai skirti spark aplinkai nustatyti
from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 12:
        raise ValueError('''Reikia visu verciu: train,trainH, test,
                testH, continuousFailas, binaryFailas,
                direktorijaSuParamGrid ,naudojamaKlasterizacija,
                naudojamaKlasfikacija, direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')

    sc = SparkContext(master="local",
          appName="ClassificationParameterSearchProcess")
    sqlContext = SQLContext(sc)

    # Surenkamos reikalingos pasirinktys pateiktos iš konsolės.
    trainDataPath = util_fnc.makePathFromPythonContext(sys.argv[1])
    trainDataHeaderPath = util_fnc.makePathFromPythonContext(sys.argv[2])
    testDataPath = util_fnc.makePathFromPythonContext(sys.argv[3])
    testDataHeaderPath = util_fnc.makePathFromPythonContext(sys.argv[4])
    continuousPath = util_fnc.makePathFromPythonContext(sys.argv[5])
    binaryPath =  util_fnc.makePathFromPythonContext(sys.argv[6])
    paramGridPath = util_fnc.makePathFromPythonContext(sys.argv[7])
    clusteringModel = sys.argv[8]
    classificationModel = sys.argv[9]
    outputPath = util_fnc.makePathFromPythonContext(sys.argv[10])
    outputFileName = sys.argv[11]

    # Patikrinama ar gauti tinkami parametrai, kitu atveju parodomas
    # klaidos pranešimas, kurį pilnai galima matyti nebent kokiam mandram,
    # unicode palaikančiam shell
    if classificationModel not in ['randomForest',
            'decisionTree', 'logisticRegression']:
        sc.stop()
        raise ValueError('''Netinkamas modelis.reikia:
                randomForest, decisionTree, logisticRegression''')
    elif clusteringModel not in ['gaussian', 'kmeans']:
        sc.stop()
        raise ValueError('''Reikia nustatyti klasterizacijos modelį:
                gaussian, kmeans''')

    # Failo stulpelių pavadinimų užkrovimas
    with open(trainDataHeaderPath) as f:
        trainDataColumns = f.read().split(",")
    with open(testDataHeaderPath) as f:
        testDataColumns = f.read().split(",")
    # Duomenų failų užkrovimas
    trainData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(trainDataPath)
        .rdd
        .toDF(trainDataColumns)
    )
    testData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(testDataPath)
        .rdd
        .toDF(testDataColumns)
    )
    # Stulpelių duomenų tipų sąrašų užkrovimas
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")

    with open(binaryPath) as f:
        binaryColumns = f.read().split(",")

    # Nustatomi tolydieji ir binariniai kintamieji esantys duomenų faile.
    # Duomenų tipų sąrašai buvo sudaryti pilniems duomenis, po to buvo
    # duomenų tvarkymo metu atmesti keli stulpeliai.
    continuousColumns = [
            c for c in continuousColumns if c in trainData.columns]
    binaryColumns = [c for c in binaryColumns if c in trainData.columns]

    # Sukuriama output direktorija skirta laikyti išeigos duomenis.
    os.makedirs(outputPath, exist_ok=True)

    # Suskaidomi duomenų rinkiniai pagal klasterius ir stratifikuojant
    # 1/4 santykiu pagal churn kintamąjį, kad viena klasė neiškreiptų
    # buvusio santykio, po to kai buvo įvykdyta klasterizacija.
    dataSets = class_fnc.makeClusterData(dataSetType = outputFileName,
                     modelType=clusteringModel, dataFile = trainData,
                     sql_context=sqlContext)
    # Sudarys 75% training ir 25% validation duomenų rinkinius iš
    # train rinkinio.
    dataSets = class_fnc.clusterTrainDataSplit(dataSets,
                     continuousColumns, binaryColumns)


    # Gaunamos nuorodos į atskiras skirtingų hiperparametrų tinklelio
    # reikšmes pagal kurias bus kuriami, treniruojami ir patikrinami modeliai
    paramGrid = util_fnc.matchParamFilenames(paramGridPath, '*.json')

    # Apskaičiuojamos visiems klasteriams klasifikacijos reikšmės
    # pagal parametro tinklelio reikšmes. Duomenys funkcijoje yra
    # stratifikuojami, jeigu nėra išlaikomas churn kintamojo klasių santykis.
    # Grąžinamas dictionary objektas su raktais, atitinkančiais kiekvieną
    # klasterį ir reikšmėm - sąrašais laikančiais pagal kiekvieną
    # hiperparametrą apskaičiuotus patikrintus rezultatus, dictionary pavidale.
    allResults = class_fnc.paramSearchForAllClusters(dataSets,
                   continuousColumns, binaryColumns, classificationModel,
                   paramGrid)
    # Rezultatų išvedimas json objekto pavidalu.
    with open(os.path.join(outputPath,
             'param_search_{0}_{1}.json'.format(classificationModel,
               clusteringModel)), 'w') as f:
        json.dump(allResults, f)

    sc.stop()
