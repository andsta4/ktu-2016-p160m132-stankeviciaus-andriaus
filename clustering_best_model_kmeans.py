# -*- coding: utf-8 -*-
# Failas clustering_best_model_selection.py
# Įkeliamas geriausias klasterizacijos modelis ir pagal jį klasterizuojami
# duomenys bei apskaičiuojamos churn kintamojo pasiskirstymo statistikos
# pagal klasterius.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext
from src.clustering_functions import saveClustersData, createClusterResultsDF, rescaleClusterCenters

from pyspark.mllib.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.clustering import KMeansModel

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 10:
        raise ValueError('''Reikia visu verciu: duomenuStandartizuotasFailas
                , toFailoHeader, churnFailas, continuousFailas,
                standartizacijoVidurkiuFailas, standartizacijosStdFailas,
                geriausioModelioPath, direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')

    sc = SparkContext(master="local[*]", 
                      appName="ClusteringWithBestModelProcess")
    sqlContext = SQLContext(sc)

    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    dataLabelPath = makePathFromPythonContext(sys.argv[3])
    continuousPath = makePathFromPythonContext(sys.argv[4])
    scalerMeanPath = makePathFromPythonContext(sys.argv[5])
    scalerStdPath = makePathFromPythonContext(sys.argv[6])
    bestModelPath = makePathFromPythonContext(sys.argv[7])
    outputPath = makePathFromPythonContext(sys.argv[8])
    outputFileName = sys.argv[9]

    # Užkraunamas duomenų rinkinio stulpelių sąrašas ir pats rinkinys.
    with open(dataHeaderPath) as f:
        dataColumns = f.read().split(",")
    allData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(dataColumns)
    )
    # Stulpelių tipų sarašų užkrovimas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")
    # Nustatomi turimi stulpeliai.
    continuousColumns = [c for c in continuousColumns if c in allData.columns]
    # Užkraunamas geriausias k-vidurkių modelis.
    bestKMeansModel = KMeansModel.load(sc, bestModelPath)   
    # Duomenys paverčiami tinkama pyspark.mllib funkcijoms forma.
    assembler = VectorAssembler(inputCols=continuousColumns, outputCol="scaled_features")
    scaledData = assembler.transform(allData)    
    # Užkraunami atsako kintamojo duomenys.
    labelData = (
    sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True, header=True, nullValue="")
        .load(dataLabelPath)
    )    
    # Sudaromas duomenų rinkinys turintis atsako kintamąjį churn.
    allData = scaledData.join(labelData.select("user_account_id", "churn"),
                              ['user_account_id'],"inner")    
    # Apskaičiuojami klasterizacijos rezultatai
    mllibKMeansClustered, churnDistributionByCluster = createClusterResultsDF(
        allData, bestKMeansModel, sqlContext, "kmeans")
    # Įrašomi rezultatai, pirma sukuriant direktoriją.
    shutil.rmtree(outputPath, ignore_errors=True)
    os.makedirs(outputPath)
    # Įrasomi klasterių pasiskirstymai į failą.
    churnDistributionByCluster.to_csv(
        os.path.join(outputPath,
                     "kmeans_clusters_composition_{}.csv".format(outputFileName)),
                     sep=',', encoding="utf-8", index=False)
    
    # Užkraunamos vidurkių statistikos apskaičiuotos standartizacijos metu.
    with open(scalerMeanPath) as f:
        meanFeatures = Vectors.dense(*map(float, f.read().split()))
    with open(scalerStdPath) as f:
        stdFeatures = Vectors.dense(*map(float, f.read().split()))
    # Atstatomos klasterių centrų vidurkių vertės ir išvedamos į failą.
    rescaledKMeansCenters = rescaleClusterCenters(bestKMeansModel,
                                                  stdFeatures, meanFeatures, 
                                                  continuousColumns, 'kmeans')
    rescaledKMeansCenters.to_csv(
        os.path.join(outputPath, "kmeans_clusters_means_{}.csv".format(outputFileName)),
        sep=',', encoding="utf-8", index=False)
    
    # Įrašomi klasterizacijos kiekvienam klasteriui rezultatai pagal
    # user_account_id ir cluster kintamuosius.
    saveClustersData(clusteringData = mllibKMeansClustered, 
        modelType = 'kmeans', dataSetType = outputFileName, fullPath = outputPath)

    sc.stop()
