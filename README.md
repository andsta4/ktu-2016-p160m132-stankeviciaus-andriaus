# Duomenu rinkiniu tyrybos metodu projektas - 25 variantas

**python3**


Job script failuose naudojamos funkcijos pateikiamos **src** direktorijoje.

Tarpiniams rezultatams įvertinti tarp job-script naudojimo yra pasitelkti exploratory.ipynb ir correlation_evaluation.ipynb, clustering_evaluation.ipynb, best_clusters_evaluation.ipynb, parameter_search_evaluation.ipynb, best_classification_evaluation.ipynb  python notebook failai.

Yra susirašyta job-script ir veiksmu seka esanti atitinkamame faile.
