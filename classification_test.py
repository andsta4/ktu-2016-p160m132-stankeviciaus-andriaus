# -*- coding: utf-8 -*-
# Failas classification_test.py
# Atliekamas klasifikacijos rezultatų patvirtinimas ant dar nematytų duomenų
# kiekvienam klasteriui atskirai ir įrašomi rezultatai json pavidalu.

import os
import sys
import json

from src.utility_functions import makePathFromPythonContext, matchParamFilenames, splitJsonParamGrid
from src.classification_functions import  makeEvaluationClusterData, bestClusterModelEvaluation

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 12:
        raise ValueError('''Reikia visu verciu: train, trainh,
                test, testh, continuousFailas, binaryFailas,
                bestParamFailas, naudojamaKlasterizacija,
                naudojamaKlasfikacija, direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')

    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local",
           appName="BestClassificationEvaluationProcess")
    sqlContext = SQLContext(sc)

    # Gaunami parametrai iš konsolės.
    trainDataPath = makePathFromPythonContext(sys.argv[1])
    trainDataHeaderPath = makePathFromPythonContext(sys.argv[2])
    testDataPath = makePathFromPythonContext(sys.argv[3])
    testDataHeaderPath = makePathFromPythonContext(sys.argv[4])
    continuousPath = makePathFromPythonContext(sys.argv[5])
    binaryPath =  makePathFromPythonContext(sys.argv[6])
    bestParamPath = makePathFromPythonContext(sys.argv[7])
    clusteringModel = sys.argv[8]
    classificationModel = sys.argv[9]
    outputPath = makePathFromPythonContext(sys.argv[10])
    outputFileName = sys.argv[11]

    if classificationModel not in ['randomForest',
            'decisionTree', 'logisticRegression']:
        sc.stop
        raise ValueError('''Reikia parinkti klasifikacijos modelį iš:
                randomForest, decisionTree, logisticRegression''')
    if clusteringModel not in ['gaussian', 'kmeans']:
        sc.stop
        raise ValueError('''Reikia parinkti klasterizacijos modelį iš:
                gaussian, kmeans''')
    # Duomenų rinkinių stulpelių pavadinimų užkrovimas.
    with open(trainDataHeaderPath) as f:
        trainDataColumns = f.read().split(",")
    with open(testDataHeaderPath) as f:
        testDataColumns = f.read().split(",")
    # Duomenų rinkinių užkrovimas
    trainData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(trainDataPath)
        .rdd
        .toDF(trainDataColumns)
    )
    testData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(testDataPath)
        .rdd
        .toDF(testDataColumns)
    )
    # Stulpeliu duomenų tipų sąrašų užkrovimas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")

    with open(binaryPath) as f:
        binaryColumns = f.read().split(",")

    # Nustatomi turimi stulpeliai esantys sąrašuose.
    continuousColumns = [c for c in continuousColumns if c in trainData.columns]
    binaryColumns = [c for c in binaryColumns if c in trainData.columns]
    # Užkraunami geriausi pasirinkto klasifikacijos modelio parametrai.
    with open(bestParamPath) as data_file:
        bestParams = json.load(data_file)

    # Suskaidomi duomenų rinkiniai pagal klasterius ir stratifikuojant
    # 1/4 santykiu pagal churn, kad butu abiejų label pakankamai.
    dataSets = makeEvaluationClusterData(dataSetType = outputFileName,
                 modelType=clusteringModel, trainData = trainData,
                 testData = testData, sql_context=sqlContext,
                 continuousColumns=continuousColumns,
                 binaryColumns=binaryColumns)

    allResults = bestClusterModelEvaluation(dataSets, continuousColumns,
                   binaryColumns, classificationModel, bestParams)

    # Sukuriama direktorija rezultatų išvedimui.
    os.makedirs(outputPath, exist_ok = True)
    # Rezultatai surašomi į failą kaip json objektas.
    with open(os.path.join(outputPath,
             'best_model_{0}_{1}_{2}.json'.format(classificationModel,
                          clusteringModel, outputFileName)), 'w') as f:
        json.dump(allResults, f)

    sc.stop()
