# -*- coding: utf-8 -*-
# Failas: clustering_model_selection_gaussian.py
# Apskaičiuojami 10 k-vidurkių klasterizacijos modelių ir apskaičiojamos
# jų klasterizacijos statistikos.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext
import src.clustering_functions as clust_fnc

from pyspark.ml.feature import VectorAssembler
from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 6:
        raise ValueError('''Reikia visų verčių: duomenuStandartizuotasFailas
                , toFailoHeader, direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')

    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]",
     appName="ClusteringModelSelectionProcess")
    sqlContext = SQLContext(sc)
    # Atrenkami parametrai iš konsolės.
    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    continuousPath = makePathFromPythonContext(sys.argv[3])
    outputPath = makePathFromPythonContext(sys.argv[4])
    outputFileName = sys.argv[5]
    # Failo stulpelių pavadinimų užkrovimas.
    with open(dataHeaderPath) as f:
        dataColumns = f.read().split(",")

    # Failo užkrovimas.
    allData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(dataColumns)
    )
    # Užkraunamas tolydžių stulpelių sąrašas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")

    # Nustatomi turimi stulpeliai.
    continuousColumns = [c for c in continuousColumns if c in allData.columns]
    # Sudaroma output direktorija rezultatų išvedimui.
    shutil.rmtree(outputPath, ignore_errors=True)
    os.makedirs(outputPath)
    # Duomenys sudaromi,kad atitiktų formatą reikalingą pyspark mllib funkcijom.
    assembler = VectorAssembler(inputCols=continuousColumns,
                  outputCol="scaled_features")
    allData = assembler.transform(allData)
    scaledFeaturesRdd = (
        allData.select("scaled_features").rdd
        .map(lambda r: r[0])
    ).cache()
    # Treniruojami 10 modelių.
    modelsKMeans = clust_fnc.trainKModels(k=10,
                     data=scaledFeaturesRdd, model="kmeans")
    # Įrasomi visi modeliai.
    clust_fnc.saveClusteringModels2(sc, outputPath,
        modelsKMeans, outputFileName + "_kmeans")
    # Skaičiuojamos kiekvieno modelio statistikos ir metrikos.
    modelStatsKMeans = clust_fnc.computeModelStatistics(modelsKMeans,
                         data=scaledFeaturesRdd, model="kmeans")
    modelMetricKMeans = clust_fnc.computeModelMetrics(modelStatsKMeans,
                          data=scaledFeaturesRdd, model="kmeans")
    # Išvedami duomenys csv formatu.
    modelStatsKMeans.to_csv(os.path.join(outputPath,
        "kmeans_statistics_{}.csv".format(outputFileName)),
            sep=',', encoding="utf-8", index=False)
    modelMetricKMeans.to_csv(os.path.join(outputPath,
        "kmeans_metrics_{}.csv".format(outputFileName)),
            sep=',', encoding="utf-8", index=False)

    sc.stop()
