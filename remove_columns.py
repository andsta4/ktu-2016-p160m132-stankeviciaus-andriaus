
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    sc = SparkContext(master="local[*]", appName="RMColumnsProcess")

    if len(sys.argv) != 6:
        sc.stop()
        raise ValueError('Reikia visu verciu: duomenuFailas, toFailoHeader, atmetamuStulpeliuSarasoFailas' +\
                        ', direktorijaKurRasytiFaila, kaipPavadintiFaila')

    sqlContext = SQLContext(sc)

    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    columnsPath = makePathFromPythonContext(sys.argv[3])
    outputPath = makePathFromPythonContext(sys.argv[4])
    outputFileName = sys.argv[5]

    # Failo header uzkrovimas
    with open(dataHeaderPath) as f:
        dataColumns = f.read().split(",")
    # Failo uzkrovimas
    allData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(dataColumns)
    )
    # Istrinamu stulpeliu atskirtu , esanciu vienoje eiluteje uzkrovimas
    with open(columnsPath) as f:
        removalColumns = f.read().split(",")
    
    # Atrenkami tinkami stulpeliai
    keptColumns = [c for c in allData.columns if c not in removalColumns]
    allData = allData.select(keptColumns)
    # Istrinamas output dir jeigu toks buvo pries tai
    shutil.rmtree(outputPath, ignore_errors=True)
    # Sukurimas output dir
    os.makedirs(outputPath)
    # Failu irasymas i nustatyta direktorija nustatytais pavadinimais
    with open(os.path.join(outputPath,"header__corr_rm_usage_{}.txt".format(outputFileName)),
              "w") as f:
        f.write(",".join(allData.columns))
    allData.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath,"corr_rm_usage_{}".format(outputFileName))
        )
    sc.stop()
