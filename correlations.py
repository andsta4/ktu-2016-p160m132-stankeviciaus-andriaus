# -*- coding: utf-8 -*-
# Failas: correlations.py
# Skirtas nustatyti pasirinktoms koreliacijoms, grąžina koreliuojančių
# tolydžių kintamųjų sąrašą.

import os
import sys

from src.utility_functions import makePathFromPythonContext
from src.feature_selection_functions import correlations

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 7:
        sc.stop()
        raise ValueError('''Reikia visu verciu: duomenuAgreguotasFailas,
                toFailoHeader, continuousStulpeliuSarasoFailas,
                direktorijaKurRasytiFaila, kaipPavadintiFaila,
                koreliacijosTipas''')
    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]", appName="CorrelationProcess")
    sqlContext = SQLContext(sc)
    # Parametrai gaunami iš konsolės.
    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    continuousPath = makePathFromPythonContext(sys.argv[3])
    outputPath = makePathFromPythonContext(sys.argv[4])
    outputFileName = sys.argv[5]
    correlationType = sys.argv[6]

    # Patikrinama parametrai yra tinkami
    if correlationType not in ['pearson', 'spearman']:
        sc.stop()
        raise ValueError('''Reikia, kad paskutinis kintamasis butu pearson
                arba spearman''')

    # Failo stulpelių pavadinimų užkrovimas.
    with open(dataHeaderPath) as f:
        columnsDataAggregated = f.read().split(",")

    # Failo užkrovimas.
    dataAggregated = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(columnsDataAggregated)
    )
    # Stulpelių tipų sąrašų užkrovimas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")

    # Nustatomi duomenų rinkinyje esantys tolygiųjų kintamųjų stulpeliai.
    continuousColumns = [
            c for c in continuousColumns if c in columnsDataAggregated]
    # Koreliacijų nustatymas pagal pasirinktą tipą.
    # Nustatomos kintamųjų poros turinčios didesnę negu 0.85 koreliaciją
    # tarpusavyje, grąžinamos poros ir jų koreliacijų reikšmės, įrašomos
    # į failą tolimesniam įvertinimui.
    if correlationType == 'pearson':
        highCorrelation = correlations(dataAggregated,
                            continuousColumns, correlationType)
        os.makedirs(outputPath, exist_ok = True)
        highCorrelation.to_csv(os.path.join(outputPath,
            "correlated_columns_pearson_{}.csv".format(outputFileName)),
            sep=',', encoding='utf-8', index=False)
    elif correlationType == 'spearman':
        highCorrelation = correlations(dataAggregated, continuousColumns,
                            correlationType)
        os.makedirs(outputPath, exist_ok = True)
        highCorrelation.to_csv(os.path.join(outputPath,
            "correlated_columns_spearman_{}.csv".format(outputFileName)),
            sep=',', encoding='utf-8', index=False)

    sc.stop()
