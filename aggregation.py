# -*- coding: utf-8 -*-
# Failas aggregation.py
# Spark job script skirtas agreguoti duomenis pagal pasirinktą modelio tipą.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext

from pyspark import SparkContext
from pyspark.sql import SQLContext

if __name__ == "__main__":
    if len(sys.argv) != 8:
        raise ValueError('''Reikia visu verciu: duomenuSutvarkytasFailas,
                toFailoHeader, continuousStulpeliuSarasoFailas,
                binaryStulpeliuSarasoFailas, direktorijaKurRasytiFaila,
                kaipPavadintiFaila, kokiamTikslui''')

    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]", appName="AggregationProcess")
    sqlContext = SQLContext(sc)
    # Parametrai iš konsolės.
    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    continuousPath = makePathFromPythonContext(sys.argv[3])
    binaryPath = makePathFromPythonContext(sys.argv[4])
    outputPath = makePathFromPythonContext(sys.argv[5])
    outputFileName = sys.argv[6]
    aggregationPurpose = sys.argv[7]
    # Patikrinami parametrai.
    if aggregationPurpose not in ['clustering', 'classification']:
        sc.stop()
        raise ValueError('''Reikia, kad paskutinis kintamasis butu clustering
                arba classification''')
    # Užkraunamas duomenų rinkinio stulpelių pavadinimų sąrašas ir
    # pats duomenų rinkinys.
    with open(dataHeaderPath) as f:
        columnsDataCleaned = f.read().split(",")
    dataCleaned = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(columnsDataCleaned)
    )
    # Stulpelių tipų sąrašų užkrovimas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")
    with open(binaryPath) as f:
        binaryColumns = f.read().split(",")
    # Nustatomi turimi stulpelių tipai.
    continuousColumns = [
            c for c in continuousColumns if c in dataCleaned.columns]
    binaryColumns = [
            c for c in binaryColumns if c in dataCleaned.columns]
    # Agregacijos instrukcijos pagal dažniausiai naudojamas statistikas.
    dataCleaned.registerTempTable("cleaned_data")
    sqlExprsMeanCols = ["AVG({0}) AS {0}".format(c) for c in continuousColumns]
    sqlExprsBinaryCols = ["MAX({0}) AS {0}".format(c) for c in binaryColumns]
    sqlExprsDateCols = ["COUNT(*) AS n_months"]
    sqlSelectExprs = sqlExprsMeanCols + sqlExprsBinaryCols + sqlExprsDateCols
    sqlSelect = ", ".join(sqlSelectExprs)
    # Agregacija pagal pasirinktą modelį per month kintamąjį.
    if aggregationPurpose == 'classification':
        dataTrain = sqlContext.sql("""
        SELECT user_account_id, {0}
        FROM cleaned_data WHERE month != 8
        GROUP BY user_account_id""".format(sqlSelect))
        dataTrain = dataTrain.drop('month')
        neededColumns = dataCleaned.columns
        neededColumns.remove("user_account_id")
        neededColumns = ','.join(neededColumns)
        dataTest = sqlContext.sql("""
        SELECT user_account_id, {0}
        FROM cleaned_data WHERE month = 8""".format(neededColumns))
        dataTest = dataTest.drop("month")
        # Surašomi duomenys.
        shutil.rmtree(outputPath, ignore_errors=True)
        os.makedirs(outputPath)
        with open(os.path.join(outputPath,
            "header__agg_usage_train_{}.txt".format(outputFileName)), "w") as f:
            f.write(",".join(dataTrain.columns))
        dataTrain.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath, "agg_usage_train_{}".format(outputFileName)))
        with open(os.path.join(outputPath, 
            "header__agg_usage_test_{}.txt".format(outputFileName)), "w") as f:
            f.write(",".join(dataTest.columns))
        dataTest.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath, "agg_usage_test_{}".format(outputFileName))
                        )
    elif aggregationPurpose == 'clustering':
        dataAll = sqlContext.sql("""
        SELECT user_account_id, {0}
        FROM cleaned_data
        GROUP BY user_account_id""".format(sqlSelect))
        dataAll = dataAll.drop("month")
        # Įrašomi duomenys.
        shutil.rmtree(outputPath, ignore_errors=True)
        os.makedirs(outputPath)
        with open(os.path.join(outputPath,
            "header__agg_usage_{}.txt".format(outputFileName)), "w") as f:
            f.write(",".join(dataAll.columns))
        dataAll.write.format("com.databricks.spark.csv").save(
            os.path.join(outputPath,"agg_usage_{}".format(outputFileName)))

    sc.stop()
