# -*- coding: utf-8 -*-
# Failas: clustering_functions.py
# Funkcijos naudojamos klasterizacijai.
import os
from scipy.stats import multivariate_normal
from pyspark.mllib.clustering import GaussianMixture, GaussianMixtureModel, KMeans, KMeansModel
from pandas.core.frame import DataFrame
import numpy as np
from .utility_functions import makePathFromPythonContext


# K vidurkių modeliams skirtos funkcijos.
def computeFK(k, SSE, prevSSE, dim):
    if k == 1 or prevSSE == 0:
        return 1
    weight = weightFactor(k, dim)
    return SSE / (weight * prevSSE)

def weightFactor(k, dim):
    if not k >= 2:
        raise ValueError("k should be greater than 1.")

    def weightFactorAccumulator(acc, k):
        if k == 2:
            return acc
        return weightFactorAccumulator(acc + (1 - acc) / 6, k - 1)

    k2Weight = 1 - 3 / (4 * dim)
    return weightFactorAccumulator(k2Weight, k)

def computeFKs(ksAndSSEPairs, dimension):
    triples = makeFkTriples(ksAndSSEPairs)
    ksWithFks = [
        (k, computeFK(k, Sk, prevSk, dimension))
        for (k, Sk, prevSk) in triples]
    return sorted(ksWithFks, key=lambda pair: pair[0])


def makeFkTriples(ksAndSSEPairs):
    sortedPairs = sorted(ksAndSSEPairs, reverse=True)
    candidates = list(zip(sortedPairs, sortedPairs[1:]))
    triples = [
        (k, SSE, prevSSE)
        for ((k, SSE), (prevK, prevSSE)) in candidates
        if k - prevK == 1
    ]
    return triples


def predictKMeans(x, centers):
    return min(
        (i for i in range(len(centers))),
        key=lambda i: (x - centers[i]).norm(2))

def addKMeansPredictionToDataFrame(df, featuresCol, predictionCol,
                                   kMeansModelCenters):
    return (
        df.rdd
        .map(lambda r: addKMeansPredictionToRow(r, featuresCol,
            kMeansModelCenters))
        .toDF(df.columns + [predictionCol])
    )

def addKMeansPredictionToRow(row, featuresCol, kMeansModelCenters):
    features = row.asDict()[featuresCol]
    return row + (predictKMeans(features, kMeansModelCenters),)


# Gauso mišinių klasterizacijai reikalingos funkcijos.

def gmmPdf(x, weights, gaussians):

    return np.sum([w * multivariate_normal.pdf(x, g.mu, g.sigma.toArray(),
                    allow_singular=True)
                   for w, g in zip(weights, gaussians)], axis=0)


def calculateLogLikelyhood(data, model):
    # reikia padaryti kopijas nes reikalingi kintamieji yra nedideli palyginus array tipo
    # o pagrindinis model yra susietas su spark-context ir dar kazka
    weights = model.weights
    gaussians = model.gaussians
    def someLog(value):
        rowLog = np.log(value)
        if rowLog == -np.inf:
            rowLog = np.log(3e-320)
        return rowLog
    logLikelyhoodOfModel = ( data.map(lambda row: someLog(gmmPdf(row.toArray(),
                                weights, gaussians)))
                            .reduce(lambda a, b: a+b)
                           )
    return logLikelyhoodOfModel


def computeBIC(n_obs, D, logLikelyhood):
    return (-2 * logLikelyhood + 2 * D * np.log(n_obs))

def computeAIC(n_obs, D, logLikelyhood):
    return (-2 * logLikelyhood + 2 * D + (2*(D+1)*(D+2)/(n_obs-D-2)))

def calculateComplexity(model):
    N_gaussians = len(model.gaussians)
    N_features = len(model.gaussians[0].mu) # pas visus tiek pat bus
    D_sigma = N_gaussians * N_features * (N_features+1)/2
    D_mu = N_features * N_gaussians
    D = int(D_sigma + D_mu + N_gaussians - 1)
    return D


def predictGaussian(x, gaussians, weights):
    probability = lambda i: weights[i] * \
        multivariate_normal.pdf(x, gaussians[i].mu,
            gaussians[i].sigma.toArray(), allow_singular=True)
    return max(
        (i for i in range(len(gaussians))),
        key=probability)

def addGaussianPredictionToDataFrame(df, featuresCol, predictionCol,
                                     gaussians, weights):
    return (
        df.rdd
        .map(lambda r: addGaussianPredictionToRow(r, featuresCol,
                                                  gaussians, weights))
        .toDF(df.columns + [predictionCol])
    )

def addGaussianPredictionToRow(row, featuresCol, gaussians, weights):
    features = row.asDict()[featuresCol]
    return row + (predictGaussian(features, gaussians, weights),)

# Bendrai klasterizacijai naudojamos funkcijos.

def trainKModels(k, data, model = "gaussian"):
    if data is None:
        raise ValueError("No data given. Must be PythonRdd.")

    ks = list(range(1, k +1 ))
    if model == "gaussian":
        kMllibModelPairs = [
            (k, GaussianMixture.train(data, k=k, seed=9001)) for k in ks
        ]
    elif model == "kmeans":
        kMllibModelPairs = [
            (k, KMeans.train(data, k=k, seed=9001)) for k in ks
        ]
    else:
        raise ValueError("model must be one of these: gaussian kmeans")

    return kMllibModelPairs

def computeModelStatistics(modelPairs, data, model= "gaussian"):
    if data is None:
        raise ValueError("No data given. Must be PythonRdd.")
    statisticTypes = {"gaussian" : ("logLikelyhood", "complexity",),
     "kmeans" : ("SSE",)}
    if model == "gaussian":
        kMllibModelStatistics = [
            (k, calculateLogLikelyhood(data, model),
             calculateComplexity(model)) for k, model in modelPairs
        ]
    elif model == "kmeans":
        kMllibModelStatistics = [
            (k, model.computeCost(data)) for k, model in modelPairs
        ]
    else:
        raise ValueError("model must be one of these: gaussian kmeans")

    kMllibStatisticsPdDf = DataFrame.from_records(
            kMllibModelStatistics, columns=["k", *statisticTypes[model]])
    return kMllibStatisticsPdDf

def computeModelMetrics(kMllibStatistics, data, model= "gaussian"):
    if data is None:
        raise ValueError("data is None. Must be PythonRdd .")

    nObs = data.count()
    dimension = len(data.first())
    metricTypes = {"gaussian" : ("BIC", "AIC",) , "kmeans" : ("fK", "Sk",)}
    if isinstance(kMllibStatistics,DataFrame):
        statisticsTuples = list(
            map(tuple, kMllibStatistics.to_records(index=False)))
    else:
        statisticsTuples = kMllibStatistics

    if model == "gaussian":
        modelMetrics = [
            (k, computeAIC(nObs, D, logLikelyhood),
             computeBIC(nObs, D,
                logLikelyhood)) for k, logLikelyhood, D in statisticsTuples
        ]
    elif model == "kmeans":
        modelFKs = computeFKs(statisticsTuples, dimension)
        a, b = list(zip(*modelFKs))
        c = list(zip(*statisticsTuples))[1]
        modelMetrics = list(zip(a, b, c))

    else:
        raise ValueError("model must be one of these: gaussian kmeans")

    kMllibMetricPdDf = DataFrame.from_records(
            modelMetrics, columns=["k", *metricTypes[model]])
    return kMllibMetricPdDf


# Modelio objektų laikymui diske skirtos funkcijos.

def saveClusteringModels(sc, directory, modelList, name= None):
    if name is None:
        raise ValueError("No name for the model files.")
    for model in modelList:
        modelName = name + "_" + str(model[0]) + "clusters"
        fullPath = makePathFromPythonContext(directory, modelName)
        model[1].save(sc, fullPath)

def saveClusteringModels2(sc, path, modelList, name= None):
    if name is None:
        raise ValueError("No name for the model files.")
    for model in modelList:
        modelName = name + "_" + str(model[0]) + "clusters"
        fullPath = os.path.join(path, modelName)
        model[1].save(sc, fullPath)

def loadClusteringModels(sc, directory, k, name = None, modelType= None):
    """

    sc:

    directory: str

    k: int max number of clusters in a model

    """
    if name is None or modelType is None:
        raise ValueError("No general name of models")

    ks = list(range(1, k +1 ))
    modelName = lambda a: name + "_" + str(a) + "clusters"
    if modelType == "gaussian":
        loadedModels = [ (k,
                GaussianMixtureModel.load(
                              sc, makePathFromPythonContext(
                              directory,modelName(k))),) for k in ks]
    elif modelType == "kmeans":
        loadedModels = [ (k,
                          KMeansModel.load(
                              sc, makePathFromPythonContext(
                              directory,modelName(k))),) for k in ks]
    return loadedModels


# Duomenų tvarkymui skirtos funkcijos.

def createClusterResultsDF(dataWithLabels, bestModel, sc, modelType = "kmeans"):
    if modelType == "kmeans":
        clusterDF = addKMeansPredictionToDataFrame(
            dataWithLabels,
            "scaled_features",
            "pred_kmeans_mllib",
            bestModel.centers)
        (
            clusterDF.select(["pred_kmeans_mllib", "churn"])
            .registerTempTable("tmp_kmeans_clustering_results")
        )
        churnDistributionByCluster = sc.sql("""
            SELECT
                pred_kmeans_mllib
                , COUNT(*) AS n_obs
                , AVG(churn) as class_balance
            FROM tmp_kmeans_clustering_results
            GROUP BY pred_kmeans_mllib
            ORDER BY n_obs DESC
            """).toPandas()
    elif modelType == "gaussian":
        clusterDF = addGaussianPredictionToDataFrame(
            dataWithLabels,
            "scaled_features",
            "pred_gaussian_mllib",
            bestModel.gaussians, bestModel.weights)
        (
            clusterDF.select(["pred_gaussian_mllib", "churn"])
            .registerTempTable("tmp_gaussian_clustering_results")
        )

        churnDistributionByCluster = sc.sql("""
        SELECT
            pred_gaussian_mllib
            , COUNT(*) AS n_obs
            , AVG(churn) as class_balance
        FROM tmp_gaussian_clustering_results
        GROUP BY pred_gaussian_mllib
        ORDER BY n_obs DESC
        """).toPandas()

    return (clusterDF, churnDistributionByCluster)


def rescaleClusterCenters(trainedModel, stdValues, meanValues,
                          continuousColumns, modelType = 'kmeans'):
    if modelType == 'gaussian':
        rescaledCenters = [
            ( cluster, *(gaussian.mu * stdValues + meanValues)) for cluster,
            gaussian in enumerate(trainedModel.gaussians)]
        rescaledCenters = DataFrame.from_records(
            rescaledCenters, columns=["cluster", *continuousColumns])
    elif modelType == 'kmeans':
        rescaledCenters = [
            ( cluster, *(means * stdValues + meanValues)) for cluster,
            means in enumerate(trainedModel.centers)]
        rescaledCenters = DataFrame.from_records(
            rescaledCenters, columns=["cluster", *continuousColumns])

    return rescaledCenters


def saveClustersToSeparateFiles(clusteringData, modelType,
                                dataSetType='sparse'):
    if modelType not in ['kmeans', 'gaussian']:
        raise ValueError('wrong modelType')

    for cluster in range(clusteringData.select(
            'pred_{0}_mllib'.format(modelType)).distinct().count()):
        clusterData = clusteringData.filter(
                        'pred_{0}_mllib={1}'.format(modelType, cluster)).drop(
                        'pred_{0}_mllib'.format(modelType))
        clusterData.write.format("com.databricks.spark.csv").save(
            makePathFromPythonContext("output", "clustering", dataSetType,
                "clustered_labels_{0}{1}/".format(modelType, cluster)))

    with open(makePathFromPythonContext("output",'clustering',dataSetType,
         "header__clustered_labels_{0}.txt".format(modelType)), "w") as f:
        f.write(",".join(clusterData.columns))

    print('Data sets written in this directory')
    print(os.listdir(makePathFromPythonContext('output', 'clustering',
            dataSetType)))
    print('Done')

def saveClustersData(clusteringData,  modelType, dataSetType='sparse',
                        fullPath = None):
    if modelType not in ['kmeans', 'gaussian']:
        raise ValueError('wrong modelType')
    if fullPath is None:
        raise ValueError('no path for saving files')
    predictionColumn = 'pred_{0}_mllib'.format(modelType)
    clusterData = (
        clusteringData.select(['user_account_id', predictionColumn])
    )
    clusterData.write.format("com.databricks.spark.csv").save(
        os.path.join(fullPath,
                     "clustered_usage_{0}_{1}/".format(modelType, dataSetType)))
    with open(os.path.join(fullPath,
        "header__clustered_usage_{0}_{1}.txt".format(modelType,
        dataSetType)), "w") as f:
        f.write(",".join(clusterData.columns))
