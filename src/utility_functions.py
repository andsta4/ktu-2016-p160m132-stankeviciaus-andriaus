# -*- coding: utf-8 -*-
# Failas: utility_functions.py
# Funkcijos su įvairia pagalbine paskirtimi.
import os
import json
import glob

def makePathFromPythonContext(*ending):
    return os.path.join(os.getcwd(), *ending)

def matchParamFilenames(dir_params, matchString="params_*.json"):
    return glob.glob(os.path.join(dir_params, matchString))  

def splitJsonParamGrid(pathJsonParamGrid, dirOutput):
    with open(pathJsonParamGrid) as fIn:
        paramGrid = json.load(fIn)
    os.makedirs(dirOutput, exist_ok=True)
    for i, p in enumerate(paramGrid):
        pathParams = os.path.join(dirOutput, "params_{:03d}.json".format(i))
        with open(pathParams, "w") as fOut:
            json.dump(p, fOut)

def convertStatsFlatMap(stats, dataType='validation'):
    rowNames = list(stats.keys())
    newData = []
    if dataType == 'validation':
        for row in rowNames:
            for i in map(lambda x: {**x['metrics'],
                **x['params'], 'cluster':row} ,stats[row]):
                newData.append(i)
    elif dataType == 'test':
        for row in rowNames:
            newData.append({**stats[row]['metrics'],
                **stats[row]['params'], 'cluster':row})        
    else:
        raise ValueError('DataType must be: validation, test')
    return newData
