# -*- coding: utf-8 -*-

# testai skirti klasifikacijos funkcijoms, nes ten belekiek laiko skaiciuoja su visais duomenim
import pytest

# this allows using the fixture in all tests in this module, executes making spark context only once
pytestmark = pytest.mark.usefixtures("spark_context", "hive_context")


# Reikalingos funkcijos ir objektai
from pyspark.sql.dataframe import DataFrame
import pyspark.mllib as mllib
import pyspark.ml as ml
import os
import random
import json

import pandas

# Testuojamos funkcijos
from classification_functions import makePathFromPythonContext
from classification_functions import matchParamFilenames

from classification_functions import makeClassificationPreprocessingPipeline
from classification_functions import makeClassifier
from classification_functions import evaluateClassification
from classification_functions import trainValidateEvaluate
from classification_functions import parameterGridSearch
from classification_functions import loadClusterData

################################################################################

## fixture objektai skirti testavimui
@pytest.fixture(scope='function', params=None, autouse=False)
def continuousColumns():
    return ['third', 'fourth']

@pytest.fixture(scope='function', params=None, autouse=False)
def binaryColumns():
    return ['first', 'second']

@pytest.fixture(scope='function', params=None, autouse=False)
def fixtureScaledDF(spark_context, hive_context): # fixtures somehow uses other fixtures without mark????
    rdd = spark_context.parallelize(
        [
        (1, 0, 0.22, -0.33, 0),
        (1, 1, -0.33, -0.45, 0),
        (1, 0, 0.35, 0.34, 0),
        (0, 1, 0.11, -0.61, 0),
        (0, 0, 0.44, 0.22, 1),
        (0, 1, -0.41, 0.25, 0),
        (0, 1, 0.12, 0.64, 0),
        (1, 0, 0.23, -0.12, 1),
        (1, 1, 0.54, 0.61, 0),
        (1, 0, -0.21, 0.31, 1),
        (1, 0, -0.21, 0.31, 0) ,
        (1, 0, 0.21, 0.21, 1) ,
        (1, 0, -0.21, 0.11, 0) ,
        (1, 0, -0.21, 0.31, 1) ,
        (1, 0, 0.21, 0.31, 0) ,
        (1, 0, -0.21, 0.31, 0) ,
        (1, 0, 0.21, 0.11, 0) ,
        (1, 1, -0.21, 0.21, 1) ,
        (0, 0, 0.21, 0.11, 0) ,
        (0, 1, -0.21, 0.31, 0) ,
        (0, 1, -0.21, 0.11, 1) ,
        (0, 1, -0.21, 0.11, 1) ,
        (1, 1, 0.21, 0.31, 0) ,
        (1, 1, -0.21, 0.11, 0) ,
        (0, 0, 0.21, 0.31, 0) ,
        (1, 1, -0.21, 0.21, 0) ,
        (1, 1, -0.21, 0.11, 0) ,
        (0, 0, 0.21, 0.31, 0) ,
        (0, 1, -0.21, 0.11, 0) ,
        (0, 0, 0.23, 0.11, 0) ,
        (0, 0, -0.21, 0.21, 0) ,
        (0, 0, 0.21, 0.31, 0) ,
        (1, 1, -0.21, 0.11, 0) ,
        (0, 1, 0.21, 0.21, 0) ,
        (0, 1, -0.21, 0.11, 0) ,
        (0, 0, 0.21, 0.21, 0) ,
        (1, 0, 0.21, 0.31, 0) ,
        (0, 1, -0.21, 0.21, 0) 
        ]
    )
    df = rdd.toDF(['first', 'second', 'third', 'fourth', 'churn'])
    return df

@pytest.fixture(scope='function', params=None, autouse=False)
def modelsParams():
    params = {'randomForest':[ {'numTrees': 10.0, 'maxDepth': 10.0},
                             {'numTrees': 20.0, 'maxDepth': 5.0} ],
            'logisticRegression': [ {'numTrees': 10.0, 'maxDepth': 10.0},
                                    {'numTrees': 20.0, 'maxDepth': 5.0} ],
            'decisionTree': [ {'numTrees': 10.0, 'maxDepth': 10.0},
                                    {'numTrees': 20.0, 'maxDepth': 5.0} ] }
    return params



############### utility funkciju testai

def test_makePathFromPythonContext():
    pathContext = makePathFromPythonContext('some', 'folder')
    assert '/home/ubuntu/synced_dir/ktu-2016-p160m132-stankeviciaus-andriaus/some/folder' == pathContext
    print('Path from python context is:')
    print(pathContext)
    print('File path')
    pathFile = os.path.realpath(__file__)
    print(pathFile)
    
@pytest.mark.usefixtures("tmpdir")
def test_matchParamFilenames(tmpdir):
    print(tmpdir)
    print(str(tmpdir))
    # create a folder with some files
    tmpdir.mkdir("mydir")
    mydir = tmpdir.join('mydir')
    # create some files in a temporary directory
    fileNames = ['file1.py', 'file2.txt', 'file3.py', 'notfile1.py', 'notfile2.py', 'notfile3.py']
    for file in fileNames:
        mydir.join(file).write('smth')
    # files written into directory
    print(mydir.listdir())
    matchedFilenames = matchParamFilenames(str(mydir), 'file*.py')
    matchedFilenames = set(map(str, matchedFilenames))
    print(matchedFilenames)
    print(mydir)
    requiredFilenames = [str(mydir.join('file1.py')), str(mydir.join('file3.py'))]
    print(requiredFilenames)
    assert (matchedFilenames.issuperset(requiredFilenames) is True and\
            matchedFilenames.issubset(requiredFilenames) is True)


# splitJsonParamGrid gal ten gerai yra


# klasifikacijai skirtu funkciju testavimas

@pytest.mark.userfixture("fixtureScaledDF", "continuousColumns", "binaryColumns")
def test_makeClassificationPreprocessingPipeline(fixtureScaledDF, continuousColumns, binaryColumns):
    preparation = makeClassificationPreprocessingPipeline(fixtureScaledDF, continuousColumns, binaryColumns)
    transformation = preparation.fit(fixtureScaledDF)
    dataPrepared = transformation.transform(fixtureScaledDF)
    requiredColumns = { 'features','first', 'second', 'third', 'fourth',
                       'first_indexed','second_indexed','label_index', 'churn'}
    assert (requiredColumns.issubset(dataPrepared.columns) is True and\
            requiredColumns.issuperset(dataPrepared.columns) is True)
    preparation = makeClassificationPreprocessingPipeline(fixtureScaledDF, continuousColumns)
    transformation = preparation.fit(fixtureScaledDF)
    dataPrepared = transformation.transform(fixtureScaledDF)
    requiredColumns = { 'features','first', 'second', 'third', 'fourth','label_index', 'churn'}
    assert (requiredColumns.issubset(dataPrepared.columns) is True and\
            requiredColumns.issuperset(dataPrepared.columns) is True)
    

def test_makeClassifier():
    classifier = makeClassifier('randomForest', {'numTrees': 10.0, 'maxDepth': 10.0})
    requiredClassifier = ml.classification.RandomForestClassifier( 
            labelCol="label_index", 
            featuresCol="features",
            seed=9001, **{'numTrees': 10, 'maxDepth': 10})
    stats = [ classifier.getLabelCol(), classifier.getFeaturesCol(),
             classifier.getNumTrees(), classifier.getMaxDepth(),
             classifier.getSeed() ]
    requiredStats = [ requiredClassifier.getLabelCol(), requiredClassifier.getFeaturesCol(),
             requiredClassifier.getNumTrees(), requiredClassifier.getMaxDepth(),
             requiredClassifier.getSeed() ]
    assert stats == requiredStats
    
    classifier = makeClassifier('logisticRegression', {'numTrees': 10.0, 'maxDepth': 10.0})
    assert classifier is None
    
    classifier = makeClassifier('decisionTree', {'numTrees': 10.0, 'maxDepth': 10.0})
    assert classifier is None
    
def test_evaluateClassification():
    pass

@pytest.mark.userfixture( "continuousColumns", "binaryColumns")
def test_trainValidateEvaluate(spark_context, continuousColumns, binaryColumns):
    # more than 10 data points are needed to split data into 3 parts
    rdd = spark_context.parallelize(
                [(random.randint(0,1), random.randint(0,1),
          random.gauss(0,1), random.gauss(0,1), random.randint(0,1)) for i in range(200)] )
    fixtureScaledDF =  rdd.toDF(['first', 'second', 'third', 'fourth', 'churn'])
    preparation = makeClassificationPreprocessingPipeline(fixtureScaledDF, continuousColumns, binaryColumns)
    transformation = preparation.fit(fixtureScaledDF)
    dataPrepared = transformation.transform(fixtureScaledDF)
    print('alldata', dataPrepared.count())
    trainingDF, validationDF, testDF = dataPrepared.randomSplit([0.4, 0.3, 0.3], seed=9001)
    print('train set', trainingDF.count())
    print('validation set', validationDF.count())
    print('test set', testDF.count())
    results = trainValidateEvaluate(trainingDF, validationDF,"randomForest", {'numTrees': 10.0, 'maxDepth': 5.0})
    print(results)
    modelClassification = makeClassifier(modelType = "randomForest", kwParams= {'numTrees': 10.0, 'maxDepth': 5.0})
    modelTrained = modelClassification.fit(trainingDF)
    requiredResults = evaluateClassification(modelTrained.transform(validationDF))
    print(requiredResults)
    assert results == requiredResults

@pytest.mark.usefixtures("tmpdir")
@pytest.mark.userfixture( "continuousColumns", "binaryColumns")
def test_parameterGridSearch(spark_context, tmpdir, continuousColumns, binaryColumns):
    ### grid
    tmpdir.mkdir("grid")
    mydir = tmpdir.join('grid')
    # create some files in a temporary directory
    for file in range(4):
        mydir.join(
            "params_{0}.json".format(file)).write(
           '{{ \"maxDepth\": {0}, \"numTrees\": {1}}}'.format(
               random.choice([5.0, 10.0]), 
               random.choice([10.0, 15.0, 20.0])
           )
        )
    ###
    classificationModel = "randomForest"
    grid =  matchParamFilenames(str(mydir), '*.json')
    print(grid)
    print('4$$$$$$$$$$$$$%#$#%')
    print(mydir)
    ### dataSets
    rdd = spark_context.parallelize(
                [(random.randint(0,1), random.randint(0,1),
          random.gauss(0,1), random.gauss(0,1), random.randint(0,1)) for i in range(200)] )
    fixtureScaledDF =  rdd.toDF(['first', 'second', 'third', 'fourth', 'churn'])
    preparation = makeClassificationPreprocessingPipeline(fixtureScaledDF, continuousColumns, binaryColumns)
    transformation = preparation.fit(fixtureScaledDF)
    dataPrepared = transformation.transform(fixtureScaledDF)
    trainingDF, validationDF, testDF = dataPrepared.randomSplit([0.4, 0.3, 0.3], seed=9001)
    
    results = parameterGridSearch(grid, trainingDF, validationDF, classificationModel)
    assert results is not None

@pytest.mark.usefixtures("tmpdir")    
@pytest.mark.userfixture( "continuousColumns", "binaryColumns")  
def test_loadClusterData(spark_context,hive_context, tmpdir, continuousColumns, binaryColumns):
    dataSetType = "sparse"
    modelType = "kmeans"
    dataFolder = tmpdir.mkdir("output").mkdir("clustering").mkdir(dataSetType)
    print(str(dataFolder))
    # create 5 cluster data files
    for cluster in range(5):
        rdd = spark_context.parallelize(
                [(random.randint(0,1), random.randint(0,1),
                random.gauss(0,1), random.gauss(0,1), random.randint(0,1)) for i in range(200)] )
        clusterData = rdd.toDF(['first', 'second', 'third', 'fourth', 'churn'])
        clusterData.write.format("com.databricks.spark.csv").save(
            os.path.join(str(dataFolder), 
                "clustered_labels_{0}{1}/".format(modelType, cluster)))
    # write header
    with open(os.path.join(str(dataFolder),
         "header__clustered_labels_{0}.txt".format(modelType)), "w") as f:
        f.write(",".join(clusterData.columns))
    print(dataFolder.listdir())
    # load data
    directoryPath = str(dataFolder)
    sql_context = hive_context # does not change anything because only read is used, just loads a bit longer
    loadedDataDic = loadClusterData(dataSetType, modelType, directoryPath, sql_context)
    # check if loaded data is of dict class
    assert type(loadedDataDic) == type(dict())
    print('#'*40)
    print(list(loadedDataDic.values()))
    print('#'*40)
    print(list(loadedDataDic.keys()))
    print('#'*40)
    print(type(list(loadedDataDic.values())[0]))
    # check if it has 0-4 integer keys
    assert sorted(loadedDataDic.keys()) == sorted(range(5))
    # check if values are DataFrame typeDataFrame
    assert sum(map(lambda x: isinstance(x, DataFrame), loadedDataDic.values())) == len(range(5))
    
    
############ clusterDataSplit
    
def generateClusterDataDic(num_cluster, spark_context):
    clusterDataDic = dict()
    for cluster in range(5):
        rdd = spark_context.parallelize(
                [(random.randint(1, 12414), random.randint(0,1), random.randint(0,1),
                random.gauss(0,1), random.gauss(0,1), random.randint(0,1)) for i in range(200)] )
        clusterData = rdd.toDF(['user_account_id','first', 'second', 'third', 'fourth', 'churn'])
        clusterDataDic[cluster] = clusterData
    return clusterDataDic

from classification_functions import clusterDataSplit

def clusterDataSplit2(clusteringData, continuousColumns, binaryColumns):
    splitClusterData = dict() 
    for clusterNum, clusterData in clusteringData.items():
        # paruosiami duomenys
        preparation = makeClassificationPreprocessingPipeline(clusterData, 
                continuousColumns = continuousColumns, binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterData)
        dataPrepared = transformation.transform(clusterData)
        trainingDF, validationDF, testDF = dataPrepared.randomSplit([0.5, 0.25, 0.25], seed=9001)
        splitClusterData[clusterNum] = {'train':trainingDF, 'cv':validationDF, 'test':testDF}
    return splitClusterData

import operator
from functools import reduce
@pytest.mark.userfixture( "continuousColumns", "binaryColumns")
def test_clusterDataSplit(spark_context, continuousColumns, binaryColumns):
    clusteringData = generateClusterDataDic(5, spark_context)
    splitClusterData = clusterDataSplit(clusteringData, continuousColumns, binaryColumns)
    # yra reikiamas kiekis dataframe split sukurta
    print(splitClusterData)
    assert sum(map(lambda x: isinstance(x, dict), splitClusterData.values())) == len(range(5))
    # patikrinti ar kievienam yra sukurta po dataframe
    assert sum(map(lambda x: isinstance(x, DataFrame), reduce(operator.add,map(lambda x: list(x.values()),splitClusterData.values())))) == len(range(5*3))
    # split buvo toki patys tai ir skaiciu skaicius turi buti toks pats
    def checkEqual(triple1, triple2):
        return triple1['test'].count() == triple2['test'].count() and\
               triple1['train'].count() == triple2['train'].count() and\
               triple1['cv'].count() == triple2['cv'].count()
    assert checkEqual(splitClusterData[1], splitClusterData[3]) is True

####   paramSearchForAllClusters
@pytest.mark.usefixtures("tmpdir")
@pytest.mark.userfixture( "continuousColumns", "binaryColumns")
def test_paramSearchForAllClustersImplementation(spark_context, tmpdir, continuousColumns, binaryColumns):
    # sukuriamas grid
    tmpdir.mkdir("grid")
    mydir = tmpdir.join('grid')
    # create some files in a temporary directory
    for file in range(4):
        mydir.join(
            "params_{0}.json".format(file)).write(
           '{{ \"maxDepth\": {0}, \"numTrees\": {1}}}'.format(
               random.choice([5.0, 10.0]), 
               random.choice([10.0, 15.0, 20.0])
           )
        )
    grid =  matchParamFilenames(str(mydir), '*.json')
    # sukuriami duomenu failai
    clusteringData = generateClusterDataDic(5, spark_context)
    splitClusterData = clusterDataSplit(clusteringData, continuousColumns, binaryColumns)
    allResults = dict()
    classificationModel = "randomForest"
    for cluster, clusterData in splitClusterData.items():
        # apskaiciuojamos modelio metrikos esant skirtingiems rezultatams
        trainingDF = clusterData['train']
        validationDF = clusterData['cv']
        results = parameterGridSearch(grid, trainingDF, validationDF, classificationModel)
        allResults['cluster' + str(cluster) + classificationModel] = results.copy()
    assert len(allResults.keys()) == 5
    assert len(allResults['cluster1randomForest']) == 4

    
### # bestClusterModelTest
@pytest.mark.userfixture( "continuousColumns", "binaryColumns")
def test_bestClusterModelTestImplementation(spark_context, continuousColumns, binaryColumns):
    # tada turint metrikas pratestuojamas geriausias modelis, treniruojant and train ir cv duomenu
    classificationModel = 'randomForest'
    clusteringData = generateClusterDataDic(5, spark_context)
    splitClusterData = clusterDataSplit(clusteringData, continuousColumns, binaryColumns)
    bestClassificationParams = {num : { "maxDepth": 10.0, "numTrees": 10.0} for num in range(5) }
    resultsTest = {}
    for cluster, clusterData in splitClusterData.items():
        trainingDF = clusterData['train']
        validationDF = clusterData['cv']
        testDF = clusterData['test']
        trainMax = trainingDF.unionAll(validationDF)
        paramsDict = bestClassificationParams[cluster]
        print('params', paramsDict)
        print('cluster', cluster)
        print('train', trainingDF.columns)
        print('cv', validationDF.columns)
        print('test', testDF.columns)
        print('trainMAx',trainMax.columns)
        metrics = trainValidateEvaluate(trainMax, testDF, classificationModel, paramsDict)
        resultsTest[cluster] = {"params": paramsDict, "metrics": metrics}
    print(resultsTest)
    # kad bent veiktu
    assert True



@pytest.mark.userfixture( "fixtureScaledDF") 
def test_stratificationImplementation(fixtureScaledDF):
    def stratification(dataSkewedLabels): 
        # funkcija stratifikuoti duomenis pagal kintamaji churn ir 2/1 santiki, kad
        # viena reiksme nedominuotu duomenu ir butu pakankamai duomenu skaiciavimam
        countLabel0 = dataSkewedLabels.filter("churn = 0").count()
        countLabel1 = dataSkewedLabels.filter("churn = 1").count()
        print("count of 0: %d" % countLabel0)
        print("count of 1: %d" % countLabel1)
        if countLabel0 > countLabel1:
            times = countLabel0 / countLabel1 
            ratio = 1/times
            if times < 4: # jeigu nedaugiau 4 kartus
                if times < 2:
                    fractions = {0: 1.0, 1: 1.0}
                else: 
                    fractions = {0: ratio * 2, 1: 1.0}
            else:
                fractions = {0: ratio * 4, 1: 1.0}
            dataStratified = dataSkewedLabels.sampleBy('churn',fractions=fractions, seed=9001)
        else:
            times = countLabel1 / countLabel0 
            ratio = 1/times
            if times < 4: # jeigu nedaugiau 4 kartus
                if times < 2:
                    fractions = {0: 1.0, 1: 1.0}
                else: 
                    fractions = {1: ratio * 2, 0: 1.0}
            else:
                fractions = {1: ratio * 4, 0: 1.0}
            dataStratified = dataSkewedLabels.sampleBy('churn',fractions=fractions, seed=9001)
        print("Fractions dictionrary")
        print(fractions)
        print("%%%%%%%%%%%%")
        print("ration")
        print(ratio)
        print("times")
        print(times)
        print("%%%%%%%%")
        return dataStratified
    
    dataSkewedLabels = fixtureScaledDF
    dataStratified = stratification(dataSkewedLabels)
    assert isinstance(dataStratified, DataFrame)

    dataChurn1 = dataStratified.filter('churn = 1')
    dataChurn0 = dataStratified.filter('churn = 0')
    weights = [0.3, 0.3, 0.4]
    splitChurn0 = dataChurn0.randomSplit(weights = weights, seed = 9001)
    splitChurn1 = dataChurn1.randomSplit(weights = weights, seed = 9001)
    # gausis lists po 3
    fullSplits = []
    for i in zip(splitChurn0, splitChurn1):
        fullSplits.append(i[0].unionAll(i[1]))

    dataSkewedLabels.show() 
    print("%%%%%%%%%%%")
    dataStratified.show()
    print("%%%%%%%%%%%")
    fullSplits[0].show()
    print("%%%%%%%")
    fullSplits[1].show()
    print("%%%%%%%%%%%")
    fullSplits[2].show()

    assert True
   
import numpy as np
@pytest.mark.userfixture( "fixtureScaledDF") 
def test_correlationImplementation(fixtureScaledDF,spark_context, hive_context):
    # reikia pytest is root repozicijos dir daryt
    with open( makePathFromPythonContext("output", "header__usage_sparse_scaled_customer_usage.txt")) as f:
        dataHeader = f.read().split(",")
    dataAll = (
            hive_context.read.format("com.databricks.spark.csv")
            .options(inferSchema=True)
            .load(makePathFromPythonContext("output", "usage_sparse_scaled_customer_usage/"))
            .rdd
            .toDF(dataHeader)
            )
    with open( makePathFromPythonContext("output", "columns_continuous__agg_usage.txt")) as f:
        columnsContinuous = f.read().split(",")
    
    # padarymas dense vector is duomenu atrinkus koreliuoti galincius kintamuosius
    dataCont = dataAll.select(columnsContinuous).rdd.map(lambda row: mllib.linalg.Vectors.dense([item for item in row]))

    ## Koreliacijos pagal pearson, nzn gal ir nereikia spearman
    correlationsDF = mllib.stat.Statistics.corr(dataCont, method= "pearson")
    #correlationsDF = mllib.stat.Statistics.corr(dataCont, method= "spearman")
    correlationsDF = pandas.DataFrame(correlationsDF, index=columnsContinuous, columns=columnsContinuous)

    # Gaunama virsutine trikampe matrica, kuri bus panaudota gauti 3 stulpeliu long formato  matrica turincia koreliacijas.
    correlationsUpper = correlationsDF.where(np.triu(np.ones(correlationsDF.shape), 1).astype(np.bool))
    correlationsLong = correlationsUpper.stack().reset_index()
    correlationsLong.columns = ["Feature1", "Feature2", "Correlation"]
    # paziurimos koreliuojantys stulepliai
    print("Multikolinearus stulpeliai pagal pearson")
    columnsCorrelated = correlationsLong.ix[np.abs(correlationsLong.Correlation) > 0.85, :]
    print(columnsCorrelated.sort_values(by="Correlation", ascending=False))
    
    # Sudaromas sarasas didziosios dalies stulpeliu kurie koreliuoja
    columnsCorrelatedList = columnsCorrelated.Feature2.to_dict()
    columnsCorrelatedList = set.union( set.difference( set(columnsCorrelatedList.values()), set(['gprs_usage'])) , set(['gprs_session_count']))
    columnsCorrelatedList = list(columnsCorrelatedList)
    # jeigu pasitaikys grps_usage, jis bus atmetamas, nes parodo ar daug buvo naudojamasi interneto paslaugomis, kas suteikia didele verte imonei.
    # yra pridedamas gprs_session_count, nes nera manoma, kad tai yra svarbus kintamasis. Tiesiog parodo ar daznai prisijungta prie internete apmokamu paslaugu,
    # nebutinai reiskia, kad buvo naudotasi uz didele suma, bet yra koreliuojantis su gprs_usage ir kitomis grps statistikomis, todel bus atmetamas.
    print("Stipriai koreliuojantys stulpeliai")
    print(columnsCorrelatedList)

    assert True

