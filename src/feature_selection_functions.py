# -*- coding: utf-8 -*-
# Failas feature_selection_functions.py
# Funkcijos padedančios atsirinkti naudingiausius kintamuosius.

import numpy as np
import pandas

from pyspark.mllib.linalg import Vectors
from pyspark.mllib.stat import Statistics

def correlations(dataAll, columnsContinuous, corrType = "pearson"):
    """
    Calculates chosen correlations between pairs of required columns
    in a data set.

    dataAll 
    columnsContinuous
    corrType
    returns pandas.DataFrame
    """
    dataCont = (
            dataAll
            .select(columnsContinuous)
            .rdd
            .map(lambda row: Vectors.dense([item for item in row]))
            )
    correlationsDF = Statistics.corr(dataCont, method= corrType)
    correlationsDF = pandas.DataFrame(correlationsDF,
            index=columnsContinuous, columns=columnsContinuous)
    correlationsUpper = correlationsDF.where(np.triu(
        np.ones(correlationsDF.shape), 1).astype(np.bool))
    correlationsLong = correlationsUpper.stack().reset_index()
    correlationsLong.columns = ["Feature1", "Feature2", "Correlation"]
    columnsCorrelated = correlationsLong.ix[np.abs(
        correlationsLong.Correlation) > 0.85, :]
    columnsCorrelated = columnsCorrelated.sort_values(
            by="Correlation", ascending=False)
    return columnsCorrelated
