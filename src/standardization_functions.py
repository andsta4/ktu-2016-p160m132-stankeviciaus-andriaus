# -*- coding: utf-8 -*-
# Failas: src/standartization_functions.py
# Funckijos skirtos duomenų tolydžių kintamųjų standartizacijai,
# atimant vidurkį ir padalinant iš standartinio nuokrypio.
import os
import shutil

from .utility_functions import makePathFromPythonContext

from pyspark.sql import Row
from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.mllib.linalg import DenseVector, Vectors

# Pagalbinės funkcijos skaičiavimam.

# Funkcijos išlaikyti reikalingą vektorių objektų klasę,
# nes naudojami metodai to neužtikrina.
def ensureDenseFeatures(featureDF, featuresCol="features"):
    columns = featureDF.columns
    idxCol = columns.index(featuresCol)
    transformedRow = Row(*columns)

    def rowColumnSparseToDense(row):
        values = list(row)
        values[idxCol] = Vectors.dense(row[idxCol].toArray())
        return transformedRow(*values)
    featureDF = featureDF.rdd.map(rowColumnSparseToDense).toDF()
    return featureDF

def rowColSparseToDense(row, colname="features"):
    columns = row.__fields__
    transformedRow = Row(*columns)
    idxCol = columns.index(colname)
    values = list(row)
    values[idxCol] = DenseVector(row[idxCol].toArray())
    return transformedRow(*values)


# Standartizacijai skirtos funkcijos

def scaleData(inputDataDF, continuousColumns, model=None):
    """
        Standardizes continuous columns from data set

        inputDataDF: array_liek
        continuousColumns: list

        (scalerModel, scaledDataFrame): tuple

    """
    if model is None:
        assembler = VectorAssembler(inputCols=continuousColumns,
                outputCol="features")
        featurizedDF = assembler.transform(inputDataDF)
        featuresEnsuredDF = ensureDenseFeatures(featurizedDF)
        scaler = StandardScaler(withMean=True, withStd=True,
                inputCol="features", outputCol="scaled_features")
        scalerModelEnsured = scaler.fit(featuresEnsuredDF)
        featurizedScaledEnsuredDF = scalerModelEnsured.transform(
                                        featuresEnsuredDF)
        # Pakeičiami stebėjimų standartizuotų duomenų vektoriai į reikiamą klasę.
        featurizedScaledEnsuredDF = (
                featurizedScaledEnsuredDF
            .rdd
            .map(lambda row: rowColSparseToDense(row=row,
                colname="scaled_features"))
        .toDF()
        )
    else:
        assembler = VectorAssembler(inputCols=continuousColumns,
                outputCol="features")
        featurizedDF = assembler.transform(inputDataDF)
        featuresEnsuredDF = ensureDenseFeatures(featurizedDF)
        scalerModelEnsured = model
        featurizedScaledEnsuredDF = scalerModelEnsured.transform(
                                        featuresEnsuredDF)
        # Pakeičiami stebėjimų standartizuotų duomenų vektoriai į reikiamą klasę.
        featurizedScaledEnsuredDF = (
                featurizedScaledEnsuredDF
            .rdd
            .map(lambda row: rowColSparseToDense(
                                row=row, colname="scaled_features"))
        .toDF()
        )

    return (scalerModelEnsured, featurizedScaledEnsuredDF)


def scaleData2(inputDataDF, continuousColumns, model=None):
    from pyspark.ml.linalg import Vectors, VectorUDT
    from pyspark.sql.functions import udf
    """
        Standardizes continuous columns from data set. Used for PySpark 2.1.0.

        inputDataDF: array_liek
        continuousColumns: list

        (scalerModel, scaledDataFrame): tuple

    """
    vectorize = udf(lambda vs: Vectors.dense(vs), VectorUDT())

    if model is None:
        assembler = VectorAssembler(inputCols=continuousColumns,
                outputCol="features")
        featurizedDF = assembler.transform(inputDataDF)
        featuresEnsuredDF = featurizedDF.withColumn("features",
                vectorize(featurizedDF["features"]))
        scaler = StandardScaler(withMean=True, withStd=True,
                inputCol="features", outputCol="scaled_features")
        scalerModelEnsured = scaler.fit(featuresEnsuredDF)
        featurizedScaledEnsuredDF = scalerModelEnsured.transform(
                                        featuresEnsuredDF)
        # Pakeičiami stebėjimų standartizuotų duomenų vektoriai į reikiamą klasę.
        featurizedScaledEnsuredDF = (
                featurizedScaledEnsuredDF
            .rdd
            .map(lambda row: rowColSparseToDense(row=row,
                colname="scaled_features"))
        .toDF()
        )
    else:
        assembler = VectorAssembler(inputCols=continuousColumns,
                outputCol="features")
        featurizedDF = assembler.transform(inputDataDF)
        featuresEnsuredDF = featurizedDF.withColumn("features",
                vectorize(featurizedDF["features"]))
        scalerModelEnsured = model
        featurizedScaledEnsuredDF = scalerModelEnsured.transform(
                                        featuresEnsuredDF)
        # Pakeičiami stebėjimų standartizuotų duomenų vektoriai į reikiamą klasę.
        featurizedScaledEnsuredDF = (
                featurizedScaledEnsuredDF
            .rdd
            .map(lambda row: rowColSparseToDense(row=row,
                colname="scaled_features"))
        .toDF()
        )

    return (scalerModelEnsured, featurizedScaledEnsuredDF)

# Nenaudojamos su Spark job funkcijos.
def writeScalingStatistics(scalerModel, dataSetType='sparse'):
    """Write mean and std statistics to models directory."""
    # sudarys kaip csv formata, su ',' atskirt vertems
    scalerMean = " ".join(map(str, scalerModel.mean))
    scalerStd = " ".join(map(str, scalerModel.std))
    with open(makePathFromPythonContext("models",
        "usage_{0}_scaler__mean.txt".format(dataSetType)), "w") as f:
        f.write(scalerMean)
    with open(makePathFromPythonContext("models",
        "usage_{0}_scaler__std.txt".format(dataSetType)), "w") as f:
        f.write(scalerStd)

def writeScaledData(inputDataDF, continuousColumns, dataSetType='sparse'):
    """"""
    # Atrenkami tiktai standartizuoti duomenys
    scaledRow = Row(*(["user_account_id"] + continuousColumns))
    aggUsageScaledDF = (
        inputDataDF.select("user_account_id", "scaled_features")
        .rdd
        .map(lambda row: scaledRow(row.user_account_id,
            *map(float, row.scaled_features.toArray())))
        .toDF()
    )
    # Standartizuotų duomenų header įrašomas į failą, kad būtų
    # galima įkelti su stulpelių pavadinimais. Taip pat įrašomas pats
    # duomenų failas Jeigu jau bus toks failas jis bus perrasomas
    with open(makePathFromPythonContext("output",
            "header__usage_{0}_scaled_customer_usage.txt".format(
                dataSetType)), "w") as f:
        f.write(",".join(aggUsageScaledDF.columns))
    shutil.rmtree(makePathFromPythonContext("output",
                "usage_{0}_scaled_customer_usage/".format(dataSetType)),
                ignore_errors=True)
    aggUsageScaledDF.write.format("com.databricks.spark.csv").save(
        makePathFromPythonContext("output",
            "usage_{0}_scaled_customer_usage/".format(dataSetType)))
