# -*- coding: utf-8 -*-
# Failas: classification_functions.py
# Klasifikacijai reikalingos funkcijos.
import glob
import json
import os

from pyspark import ml
from pyspark import mllib
import pyspark.ml.feature
import pyspark.ml.clustering
import pyspark.ml.classification
import pyspark.ml.evaluation
import pyspark.mllib.feature
import pyspark.mllib.clustering
import pandas as pd
import re

from .utility_functions import makePathFromPythonContext
from .utility_functions import matchParamFilenames
from .utility_functions import splitJsonParamGrid

def makeClassificationPreprocessingPipeline(dataFrame ,
                                            continuousColumns = None,
                                            binaryColumns = None):
    if binaryColumns is not None:
        classificationFeatureColumns = continuousColumns + binaryColumns
        indexedBinaryFeatureColumns = [
            c + "_indexed"
            for c in binaryColumns]
        indexingStages = ([
            ml.feature.StringIndexer(inputCol=col, outputCol=colIndexed)
            for col, colIndexed in zip(binaryColumns,
                                       indexedBinaryFeatureColumns)
            ] + [ml.feature.StringIndexer(inputCol="churn",
                                          outputCol="label_index")])
        indexingPipeline = ml.pipeline.Pipeline(stages=indexingStages)
        classificationFeatureColumns = continuousColumns +\
                                       indexedBinaryFeatureColumns
        classificationAssembler = ml.feature.VectorAssembler(
            inputCols=classificationFeatureColumns,
            outputCol="features")
        classificationFeaturizationPipeline = ml.pipeline.Pipeline(stages=[
            indexingPipeline,
            classificationAssembler])
    else:
        classificationFeatureColumns = continuousColumns
        classificationAssembler = ml.feature.VectorAssembler(
                inputCols=classificationFeatureColumns,
                outputCol="features")
        indexingStages = ([ml.feature.StringIndexer(inputCol='churn',
                            outputCol="label_index")])
        indexingPipeline = ml.pipeline.Pipeline(stages=indexingStages)
        classificationFeaturizationPipeline = ml.pipeline.Pipeline(stages=[
            indexingPipeline,
            classificationAssembler])

    return classificationFeaturizationPipeline


def makeClassifier(modelType, kwParams):
    def ensureInteger(kwParams):
        ensuredParams = ['maxIter', 'maxDepth', 'iterations', 'numTrees',
                         'maxMemoryInMB', 'minInfoGain'
                        'minInstancesPerNode', 'maxBins', 'blockSize']
        check = lambda z,x: int(x) if z in ensuredParams else x
        return { s: check(s, t) for (s, t) in kwParams.items()}
    if modelType is None:
        raise ValueError("""Must specify model type. Type can be one of :
                randomForest, decisionTree, logisticRegression """)
    ensuredParams = ensureInteger(kwParams)
    if modelType == "randomForest":
        return ml.classification.RandomForestClassifier(
            labelCol="label_index",
            featuresCol="features",
            impurity='gini',
            seed=9001, **ensuredParams)
    elif modelType == "logisticRegression":
        return ml.classification.LogisticRegression(
            labelCol="label_index",
            featuresCol="features",
            threshold=0.5,
            standardization=False,
            **ensuredParams)
    elif modelType == "decisionTree":
        return ml.classification.DecisionTreeClassifier(
            labelCol="label_index",
            featuresCol="features",
            impurity='gini',
            **ensuredParams)
    elif modelType == 'gradientTree':
        return ml.classification.GBTClassifier(
            labelCol="label_index",
            featuresCol="features",
            lossType='logistic',
            **ensuredParams)
    else:
        raise ValueError("""Model type is not available.
         Select one of: randomForest, decisionTree, logisticRegression """)

def evaluateClassification(predictionDf):
    auRocBinaryEvaluator = ml.evaluation.BinaryClassificationEvaluator(
        rawPredictionCol="rawPrediction",
        labelCol="label_index",
        metricName="areaUnderROC")
    multiPrecEvaluator = ml.evaluation.MulticlassClassificationEvaluator(
        predictionCol="prediction",
        labelCol="label_index",
        metricName="precision")
    multiRecEvaluator = ml.evaluation.MulticlassClassificationEvaluator(
        predictionCol="prediction",
        labelCol="label_index",
        metricName="recall")
    multiF1Evaluator = ml.evaluation.MulticlassClassificationEvaluator(
        predictionCol="prediction",
        labelCol="label_index",
        metricName="f1")
    return {
        "auc": auRocBinaryEvaluator.evaluate(predictionDf),
        "precision": multiPrecEvaluator.evaluate(predictionDf),
        "recall": multiRecEvaluator.evaluate(predictionDf),
        "f1": multiF1Evaluator.evaluate(predictionDf)
    }


def trainValidateEvaluate(trainingDf, validationDf, modelType,paramsDict ):
    """trains and returns model"""
    modelClassification = makeClassifier(modelType = modelType, kwParams=paramsDict)
    modelTrained = modelClassification.fit(trainingDf)
    return evaluateClassification(modelTrained.transform(validationDf))


def bootstrapCV(trainCVDF,modelType,paramsDict, numResamples):
    """bootstrap cross validation"""
    metrics = list()
    for i in range(numResamples):
        bootstrapTrain = trainCVDF.sample(
            withReplacement = True, fraction = 0.632)
        bootstrapTest = trainCVDF.filter(
            boostrapTrain['user_account_id'] != trainCVDF['user_account_id'])
        modelClassification = makeClassifier(
            modelType = modelType, kwParams=paramsDict)
        modelTrained = modelClassification.fit(bootstrapTrain)
        metrics.append(
            evaluateClassification(modelTrained.transform(bootstrapTest)))
    def avg(resamples):
        auc = 0
        f1 = 0
        precision = 0
        recall = 0
        for resample in allResamples:
            auc += allResamples['auc']
            f1 += allResamples['f1']
            precision += allResamples['precision']
            recall += allResamples['recall']
        return {'auc': auc/len(allResamples),
                'f1': f1/len(allResamples),
               'precision': precision/len(allResamples),
                'recall':recall/len(allResamples)}
    return avg(metrics)


def kFoldCV(trainCVDF,model, k=10):
    paramGrid = ml.tuning.ParamGridBuilder().build()
    evaluator = ml.evaluation.BinaryClassificationEvaluator()
    crossval = ml.tuning.CrossValidator(estimator = model,
            estimatorParamMaps = paramGrid, evaluator = evaluator, numFolds = k)
    return crossval


def parameterGridSearch(grid, trainingDF, validationDF, classificationModel):
    results = []
    for paramsPath in grid:
        with open(paramsPath) as f:
            paramsDict = json.load(f)
        metrics = trainValidateEvaluate(trainingDF, validationDF,
                    classificationModel, paramsDict )
        results.append({"params": paramsDict, "metrics": metrics})
    return results


def loadClusterData(dataSetType, modelType, directoryPath, sql_context):
    with open(os.path.join(directoryPath,
        "header__clustered_labels_{0}.txt".format(modelType))) as f:
        clustersDataFilesHeader = f.read().split(",")
    clustersDataFiles = matchParamFilenames(
        directoryPath, 'clustered_labels_{0}*'.format(modelType))
    clusterData = dict()
    for cluster in clustersDataFiles:
        clusterDF = (
            sql_context.read.format("com.databricks.spark.csv")
            .options(inferSchema=True)
            .load(cluster)
            .rdd
            .toDF(clustersDataFilesHeader)
        )
        clusterData[int(
            re.findall('\\d+', os.path.basename(cluster))[0])] = clusterDF
    return clusterData


def makeClusterData(dataSetType, modelType, dataFile, sql_context):
    clusterData = dict()
    for cluster in dataFile.select(
        ['pred_{}_mllib'.format(modelType)]).distinct().rdd.map(
                            lambda r: r[0]).collect():
        clusterDF = dataFile.filter(
                'pred_{0}_mllib = {1}'.format(modelType, cluster))
        clusterData[cluster] = clusterDF
    return clusterData


def clusterDataSplit(clusteringData, continuousColumns, binaryColumns):
    splitClusterData = dict()
    for clusterNum, clusterData in clusteringData.items():
        preparation = makeClassificationPreprocessingPipeline(clusterData,
                continuousColumns = continuousColumns,
                binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterData)
        dataPrepared = transformation.transform(clusterData)
        trainingDF, validationDF, testDF = dataPrepared.randomSplit(
                                [0.5, 0.25, 0.25], seed=9001)
        splitClusterData[clusterNum] = {'train':trainingDF,
                            'cv':validationDF, 'test':testDF}
    return splitClusterData


def stratification(dataSkewedLabels):
    countLabel0 = dataSkewedLabels.filter("churn = 0").count()
    countLabel1 = dataSkewedLabels.filter("churn = 1").count()

    if countLabel0 > countLabel1:
        times = countLabel0 / countLabel1
        ratio = 1/times
        if times < 4:
            if times < 2:
                fractions = {0: 1.0, 1: 1.0}
            else:
                fractions = {0: ratio * 2, 1: 1.0}
        else:
            fractions = {0: ratio * 4, 1: 1.0}
        dataStratified = dataSkewedLabels.sampleBy(
                            'churn',fractions=fractions, seed=9001)
    else:
        times = countLabel1 / countLabel0
        ratio = 1/times
        if times < 4:
            if times < 2:
                fractions = {0: 1.0, 1: 1.0}
            else:
                fractions = {1: ratio * 2, 0: 1.0}
        else:
            fractions = {1: ratio * 4, 0: 1.0}
        dataStratified = dataSkewedLabels.sampleBy(
                            'churn',fractions=fractions, seed=9001)
    return dataStratified


def makeEvaluationClusterData(dataSetType, modelType, trainData,
                              testData, sql_context, continuousColumns,
                              binaryColumns):
    clusterData = dict()
    for cluster in trainData.select(
        ['pred_{}_mllib'.format(modelType)]).distinct().rdd.map(
                    lambda r: r[0]).collect():
        clusterTrainData = trainData.filter(
                    'pred_{0}_mllib = {1}'.format(modelType, cluster))
        clusterTestData = testData.filter(
                    'pred_{0}_mllib = {1}'.format(modelType, cluster))
        clusterTrainData = stratification(clusterTrainData)
        clusterTestData = stratification(clusterTestData)
        preparation = makeClassificationPreprocessingPipeline(clusterTrainData,
                continuousColumns = continuousColumns,
                 binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterTrainData)
        clusterTrainData= transformation.transform(clusterTrainData)
        preparation = makeClassificationPreprocessingPipeline(clusterTestData,
                continuousColumns = continuousColumns,
                 binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterTestData)
        clusterTestData= transformation.transform(clusterTestData)
        clusterData[cluster] = {'train': clusterTrainData,
                                'test': clusterTestData}
    return clusterData


def clusterTrainDataSplit(clusteringData, continuousColumns, binaryColumns):
    splitClusterData = dict()
    for clusterNum, clusterData in clusteringData.items():
        clusterData = stratification(clusterData)
        preparation = makeClassificationPreprocessingPipeline(clusterData,
                continuousColumns = continuousColumns,
                binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterData)
        dataPrepared = transformation.transform(clusterData)
        trainingDF, validationDF = dataPrepared.randomSplit(
                                    [0.75, 0.25], seed=9001)
        splitClusterData[clusterNum] = {'train':trainingDF, 'cv':validationDF}
    return splitClusterData


def clusterTestDataSplit(clusteringData, continuousColumns, binaryColumns):
    splitClusterData = dict()
    for clusterNum, clusterData in clusteringData.items():
        clusterData = stratification(clusterData)
        preparation = makeClassificationPreprocessingPipeline(clusterData,
                continuousColumns = continuousColumns,
                 binaryColumns = binaryColumns)
        transformation = preparation.fit(clusterData)
        dataPrepared = transformation.transform(clusterData)
        splitClusterData[clusterNum] = {'test':clusterData}
    return splitClusterData


def paramSearchForAllClusters(splitClusterData, continuousColumns,
                              binaryColumns,
                              classificationModel, grid):
    allResults = dict()
    for cluster, clusterData in splitClusterData.items():
        trainingDF = clusterData['train']
        validationDF = clusterData['cv']
        results = parameterGridSearch(grid, trainingDF,
                    validationDF, classificationModel)
        allResults['cluster' + str(cluster) + classificationModel] = results.copy()
    return allResults


def bestClusterModelTest(splitClusterData, continuousColumns, binaryColumns,
                         classificationModel, bestClassificationParams):
    resultsTest = {}
    for cluster, clusterData in splitClusterData.items():
        trainingDF = clusterData['train']
        validationDF = clusterData['cv']
        testDF = clusterData['test']
        trainMax = trainingDF.unionAll(validationDF)
        paramsDict = bestClassificationParams[cluster]
        metrics = trainValidateEvaluate(trainMax, testDF,
                    classificationModel, paramsDict)
        resultsTest[cluster] = {"params": paramsDict, "metrics": metrics}
    return resultsTest


def bestClusterModelEvaluation(splitClusterData, continuousColumns,
                               binaryColumns, classificationModel,
                               bestClassificationParams):
    resultsTest = {}
    for cluster, clusterData in splitClusterData.items():
        trainingDF = clusterData['train']
        testDF = clusterData['test']
        paramsDict = bestClassificationParams
        metrics = trainValidateEvaluate(trainingDF, testDF,
                    classificationModel, paramsDict)
        resultsTest[cluster] = {"params": paramsDict, "metrics": metrics}
    return resultsTest
