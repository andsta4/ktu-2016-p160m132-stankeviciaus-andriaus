# -*- coding: utf-8 -*-
# Failas standartization_clustering.py
# Spark job python script skirtas standartizuoti/normuoti duomenų rinkinį
# skirtą klasterizacijai.
import os
import sys
import shutil

from src.utility_functions import makePathFromPythonContext
from src.standardization_functions import scaleData, scaleData2,writeScaledData

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import Row

if __name__ == "__main__":
    if len(sys.argv) != 6:
        raise ValueError('''Reikia visu verciu: duomenuFailas, toFailoHeader,
                continuousStulpeliuSarasoFailas, direktorijaKurRasytiFaila,
                kaipPavadintiFaila''')
    # Spark aplinkos nustatymai.
    sc = SparkContext(master="local[*]", appName="StandardizationProcess")
    sqlContext = SQLContext(sc)
    # Parametrai iš konsolės.
    dataPath = makePathFromPythonContext(sys.argv[1])
    dataHeaderPath = makePathFromPythonContext(sys.argv[2])
    continuousPath = makePathFromPythonContext(sys.argv[3])
    outputPath = makePathFromPythonContext(sys.argv[4])
    outputFileName = sys.argv[5]

    # Užkraunamas agreguotas, duomenų rinkinys.
    with open(dataHeaderPath) as f:
        dataColumns = f.read().split(",")
    allData = (
        sqlContext.read.format("com.databricks.spark.csv")
        .options(inferSchema=True)
        .load(dataPath)
        .rdd
        .toDF(dataColumns)
    )
    # Stulpelių tipų sarašų užkrovimas.
    with open(continuousPath) as f:
        continuousColumns = f.read().split(",")
    # Nustatomi turimi stulpeliai
    continuousColumns = [c for c in continuousColumns if c in allData.columns]
    
    # Sutvarkoma direktorija duomenų įrašymui.
    shutil.rmtree(outputPath, ignore_errors=True)
    os.makedirs(outputPath)
    # Atliekama standartizacija. Priklausomai nuo Spark versijos.
    if sc.version == '2.1.0':
        (scalerModel, scaledData) = scaleData2(allData, continuousColumns)
    else:
        (scalerModel, scaledData) = scaleData(allData, continuousColumns)
    
    # Įrašomos standartizuotų kintamųjų statistikos.
    scalerMean = " ".join(map(str, scalerModel.mean))
    scalerStd = " ".join(map(str, scalerModel.std))
    with open(makePathFromPythonContext(outputPath,
        "usage_{0}_scaler__mean.txt".format(outputFileName)), "w") as f:
        f.write(scalerMean)
    with open(os.path.join(outputPath,
        "usage_{0}_scaler__std.txt".format(outputFileName)), "w") as f:
        f.write(scalerStd)
        
    # Sutvarkomas duomenų formatas.
    scaledRow = Row(*(["user_account_id"] + continuousColumns))
    scaledData = (
        scaledData
        .select("user_account_id", "scaled_features")
        .rdd
        .map(lambda row: scaledRow(row.user_account_id,
            *map(float, row.scaled_features.toArray())))
        .toDF()
    )
    # Įrašomi duomenys.
    with open(os.path.join(outputPath,
            "header__usage_scaled_{}.txt".format(outputFileName)), "w") as f:
        f.write(",".join(scaledData.columns))
    scaledData.write.format("com.databricks.spark.csv").save(
        os.path.join(outputPath,"usage_scaled_{}/".format(outputFileName)))

    sc.stop()
